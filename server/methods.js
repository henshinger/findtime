var Future = Npm.require("fibers/future")
Meteor.methods({
    'findFriends': function() {        
        if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          FBGraph.get('/me/friends', function(err,result) {
              if (!err) {
                arr = _.map(result.data, function(obj) {
                  return _.extend(obj, {
                      src: "http://graph.facebook.com/" + obj.id + "/picture/",                
                  });
                })
                future.ret(arr);          
              }         
          });
          return future.wait();
        }else{
            return false;
        }
    },
    'findRealFriends': function () {      
      var query = "SELECT uid, name FROM user WHERE is_app_user AND uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
      if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          FBGraph.fql(query, function(err,result) {
              if (!err) {
                arr = _.map(result.data, function(obj) {
                  return {
                      id: "" + obj.uid,
                      name: obj.name,
                      src: "http://graph.facebook.com/" + obj.uid + "/picture/",                
                  };
                });  
                future.ret(arr);          
              }         
          });
          return future.wait();
        }else{
            return false;
        }
    },
    'findCityMates': function () {
      var query = "SELECT uid, name FROM user WHERE not(is_app_user) AND uid IN (SELECT uid FROM friendlist_member WHERE flid IN (select flid from friendlist WHERE owner = me() and type = 'current_city')) LIMIT 30";
      if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          FBGraph.fql(query, function(err,result) {
                if (!err) {                  
                  arr = _.map(result.data, function(obj) {
                    return {
                        id: "" + obj.uid,
                        name: obj.name,
                        src: "http://graph.facebook.com/" + obj.uid + "/picture/",                
                    };
                  });  
                  future.ret(arr);          
                }         
            });
            return future.wait();
          }else{
              return false;
          }
    },
    'findGroups': function () {
      var query = "select gid, name from group where gid in (select gid from group_member where uid = me() and bookmark_order < 16 )";
      if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          // FBGraph.get("search?type=user&q=john", function(err,result) {
          //     if (!err)
          //       future.ret(result);          
          // })
          FBGraph.fql(query, function(err, res) {
            future.ret(res.data);
          })
          return future.wait();
        }else{
            return false;
        }
    },
    'filterFriends': function (q) {
        var query = "SELECT name, uid FROM user WHERE uid IN(SELECT uid2 FROM friend WHERE uid1 = me()) AND strpos(lower(name), lower('" + q + "')) >=0 LIMIT 10";
        if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          // FBGraph.get("search?type=user&q=john", function(err,result) {
          //     if (!err)
          //       future.ret(result);          
          // })
          FBGraph.fql(query, function(err, res) {
            future.ret(res.data);
          })
          return future.wait();
        }else{
            return false;
        }
    },
    'filterGroups': function (q) {
        var query = "select gid, name from group where gid in (select gid from group_member where uid = me()) AND strpos(lower(name), lower('" + q + "')) >=0 LIMIT 15";
        if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          // FBGraph.get("search?type=user&q=john", function(err,result) {
          //     if (!err)
          //       future.ret(result);          
          // })
          FBGraph.fql(query, function(err, res) {
            future.ret(res.data);
          })
          return future.wait();
        }else{
            return false;
        }
    },
    'findGroupMembers': function(id) {
      var query = "SELECT uid, name FROM user WHERE is_app_user AND uid IN (SELECT uid FROM group_member WHERE gid = " + id + ")"
      if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          FBGraph.fql(query, function(err,result) {
                if (!err) {                  
                  arr = _.map(result.data, function(obj) {
                    return {
                        id: "" + obj.uid,
                        name: obj.name,
                        src: "http://graph.facebook.com/" + obj.uid + "/picture/",                
                    };
                  });  
                  future.ret(arr);          
                }         
            });
            return future.wait();
          }else{
              return false;
          }
    },
    'validateFriend': function(id){
      var query = "SELECT uid2 FROM friend WHERE uid1 = me() and uid2 = " + id;
      if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          // FBGraph.get("search?type=user&q=john", function(err,result) {
          //     if (!err)
          //       future.ret(result);          
          // })
          FBGraph.fql(query, function(err, res) {
            future.ret(res.data.length > 0);
          })
          return future.wait();
        }else{
            return false;
        }
    },
    'newEvent': function (obj, arr) {  
      if(Meteor.user().services.facebook.accessToken) {
          FBGraph.setAccessToken(Meteor.user().services.facebook.accessToken);
          var future = new Future();          
          //Async Meteor (help from : https://gist.github.com/possibilities/3443021
          // FBGraph.get("search?type=user&q=john", function(err,result) {
          //     if (!err)
          //       future.ret(result);          
          // })
          FBGraph.post("/me/events", obj, function(err, res) {
            console.log(err)
            if(!err) {
              console.log(res);
              FBGraph.post(res.id + "/invited?users=" + arr.join(","), function(err, result) {
                future.ret(result);
                console.log(result);
              })
            }
          });
          return future.wait();
        }else{
            return false;
        }    
      
    }, 'queryDLSU': function (user, pass) {
        var future = new Future();          
        this.unblock();
        var result = Meteor.http.call("POST", "http://enroll.dlsu.edu.ph/dlsu/user_login_p", {
          followRedirects: true,
          params: {
            V_USER_ID: user,
            V_PASSWORD: pass
          }
        });        
        if (result.statusCode === 200) {
          var firstIndex = result.content.search("px="),
            lastIndex = result.content.indexOf('>', firstIndex) - 1,
            sessionId = result.content.substring(firstIndex, lastIndex),
            schedURL = 'http://enroll.dlsu.edu.ph/dlsu/view_class_schedule?' + sessionId + '&p_sy_term=20132&p_button=Submit&p_sy_term=&p_student_no=' + user + '&p_button=Submit',
            schedPage = Meteor.http.call("POST", schedURL, {
              followRedirects: true,
            }),
            fIndex = schedPage.content.indexOf("<TABLE  cell"),
            lIndex = schedPage.content.indexOf("TABLE>", fIndex) + 6;
          future.ret(schedPage.content.substring(fIndex, lIndex));
        } else {
          future.ret("error");
        }
        
        // future.ret(result.content.substring(firstIndex, lastIndex));
        return future.wait();
    },
    'updateSchedule': function (schedId, data) {
      if (Meteor.user().services.facebook.id === data.userId) {
        Schedules.update(schedId, data);
      }
    },
    'insertSchedule': function() {
      Schedules.insert({
          dates: false,
          recurring: true,
          week: false,
          userId: Meteor.user().services.facebook.id,
          days: {
            Mon: {
              blocks: [],
              inWeek: 1
            },
            Tue: {
              blocks: [],
              inWeek: 2
            },
            Wed: {
              blocks: [],
              inWeek: 3
            },
            Thu: {
              blocks: [],
              inWeek: 4
            },
            Fri: {
              blocks: [],
              inWeek: 5
            },
            Sat: {
              blocks: [],
              inWeek: 6
            }
          }
        });
    }
    //,
    // 'sendMessage': function (msg, userId) {
    //   var fbUID = Meteor.user().services.facebook.id;
    //   var params = {
    //       jid: '-' + fbUID + '@chat.facebook.com', 
    //       api_key: '517570874963582', // api key of your facebook app
    //       access_token: Meteor.user().services.facebook.accessToken, // user access token
    //       host: 'chat.facebook.com',
    //   };
    //   var cl = new xmpp.Client(params);      
                               
    //   cl.send(new xmpp.Element('message', { to: '-' + userId + '@chat.facebook.com', type: 'chat'}).c('body').t(msg));
   
    //   cl.end();                     
    //   cl.addListener('error',
    //                  function(e) {
    //                      console.log(e);                         
    //                  });
    //   // fbchat.addListener('connected', function() {
    //   //   console.log('Connected!');


    //   //   fbchat.getFriends(function (result) {
    //   //     console.log('Friends loaded - callback !');
    //   //   });

    //   //   fbchat.whoAmI(function (result) {
    //   //     console.log('Who am i?');
    //   //     console.log(result.name);
    //   //   });

    //   //   /*
    //   //     console.log("sending");
    //   //     fbchat.sendMessage(123456, 'Hi friend!');
    //   //   */
    //   // });

    //   // fbchat.addListener('authenticationError', function(e) {
    //   //   console.log('Auth error: ' + e);
    //   // });
    //   // fbchat.close();
    // }
});