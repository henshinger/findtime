var Future = Npm.require("fibers/future")
Accounts.onCreateUser(function(options, user) {
  if (options.profile) {
    user.profile = options.profile;
  }
  if(user.services.facebook.accessToken) {
      FBGraph.setAccessToken(user.services.facebook.accessToken);
      //Async Meteor (help from : https://gist.github.com/possibilities/3443021
      	var future = new Future();
      FBGraph.get('/me?fields=education',function(err,result) {   		   
           future.ret(result.education);
      });   
   }
   user.profile.education = future.wait();
   // console.log(user);
   //console.log(options);
  return user;
});