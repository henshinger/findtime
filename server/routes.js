Meteor.Router.add('/manifest:type', function(type) {
  if (type == ".webapp") {
    return [200, {"content-type":"application/x-web-app-manifest+json"}, JSON.stringify({"verification_key": "3ec6493f-3f9c-4dd9-957d-d61a7ece7b53"})];
  } else {
    return [200, {"content-type":"application/json"}, JSON.stringify({"verification_key": "3ec6493f-3f9c-4dd9-957d-d61a7ece7b53"})];
  }
});
