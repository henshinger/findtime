Meteor.startup(function () {
  Accounts.loginServiceConfiguration.remove({
    service: "facebook"
  });
  
  Accounts.loginServiceConfiguration.insert({
    service: "facebook",
    appId: 517570874963582,
    secret: "c3af73a54bd853af8ac255749fdd9e90"
  });  

  var connect;
  connect = Npm.require("connect");
  __meteor_bootstrap__.app.use(connect.query()).use(function(req, res, next) {
    // Need to create a Fiber since we're using synchronous http
    // calls and nothing else is wrapping this in a fiber
    // automatically
    return Fiber(function() {
      if(req.url === "/channel.html") {
        res.writeHead(200, {
          'Content-Type': 'text/html'
        });
        return res.end('');
      } else {
        return next(); // if not a channel.html request, pass to next middleware.
      }
    }).run();
  });
});  
