//This is similar to $(document).ready(function(){});, but for Meteor
//Most of the time, you should place jQuery in Template.[name].rendered or in events.
//Meteor re-renders everything when data changes, so stuff will be detached if you run them here.

Meteor.startup(function(){
  
});