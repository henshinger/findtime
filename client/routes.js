//router adds routes as specified below.
//It can return a name(string) that renders the template with that name.
//It can also return a function that returns a string.
var setUser = function () {
  Session.setDefault("selectedFriends", [{
      id: Meteor.user().services.facebook.id,
      title: Meteor.user().profile.name,
      src: "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/"
    }]);
}
Meteor.Router.add({
  '/supersentai/': function() {
    //"{"_id":"2afdQ7cTHatCrZA3f","services":{"facebook":{"accessToken":"CAAHWulwl9n4BAMeaxSbYoGDJVXzktVIm5Wvu03tBjXwCuK92OTvb635hOdvQ9jzdfiv5vrjUvJrSfnV7tp1aXulHXqFaWziFsJag5GPBe6yWtcdTqj4Ib8O8Ey1FuBxfCE6uiGm5psXjTcKh","email":"kenleytan@yahoo.com","expiresAt":1379174023821,"first_name":"Kenley","gender":"male","id":"1266620128","last_name":"Tan","link":"http://www.facebook.com/kenley.earl.tan","locale":"en_US","name":"Kenley Tan","username":"kenley.earl.tan"}},"profile":{"name":"Kenley Tan","education":[{"school":{"id":"102148113160950","name":"St. Jude Catholic School"},"type":"High School"},{"school":{"id":"104191776282661","name":"Saint Jude Catholic School"},"year":{"id":"136328419721520","name":"2009"},"type":"High School"},{"school":{"id":"109345452425901","name":"Ateneo de Manila University"},"type":"College"}]}}"
    if (Meteor.user()){
      return "home";
    }else {
      return "landingPage";
    }
  },
  '/supersentai/school/': function(){    
    var school = _.findWhere(Meteor.user().profile.education, {type: "College"}).school;
    Session.set("school", school);
    Session.set("School", school.name);
    return 'signup';
  },
  '/import/:enc': function(enc) {
    var dec = Days.parse(JSON.parse(atob(enc)));
    console.log(dec);
    if (Meteor.user()) {
      var sched = Schedules.findOne({userId: Meteor.user().services.facebook.id});
      dec.userId = Meteor.user().services.facebook.id;  
      var serialized = dec.serialize();
      Schedules.update({_id: sched._id}, serialized);
      return 'myschedule';
    }    
  },  
  '/thanks': 'thanks',
  '/importer': 'importer',
  '/supersentai/testing':'newHome',
  '/supersentai/rewrite': 'newHome',
  '/supersentai/testing/kenley': function () {
    Meteor.user = function () {
      return {"_id":"2afdQ7cTHatCrZA3f","services":{"facebook":{"accessToken":"CAAHWulwl9n4BAMeaxSbYoGDJVXzktVIm5Wvu03tBjXwCuK92OTvb635hOdvQ9jzdfiv5vrjUvJrSfnV7tp1aXulHXqFaWziFsJag5GPBe6yWtcdTqj4Ib8O8Ey1FuBxfCE6uiGm5psXjTcKh","email":"kenleytan@yahoo.com","expiresAt":1379174023821,"first_name":"Kenley","gender":"male","id":"1266620128","last_name":"Tan","link":"http://www.facebook.com/kenley.earl.tan","locale":"en_US","name":"Kenley Tan","username":"kenley.earl.tan"}},"profile":{"name":"Kenley Tan","education":[{"school":{"id":"102148113160950","name":"St. Jude Catholic School"},"type":"High School"},{"school":{"id":"104191776282661","name":"Saint Jude Catholic School"},"year":{"id":"136328419721520","name":"2009"},"type":"High School"},{"school":{"id":"109345452425901","name":"Ateneo de Manila University"},"type":"College"}]}};
    }
    return 'home';
  },
  '/settings': function () {
      Session.set("selectedFriends", [{
        id: Meteor.user().services.facebook.id,
        title: Meteor.user().profile.name,
        src: "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/"
      }]);
      return 'settings'
  },
  '/myschedule': function () {
      Session.set("selectedFriends", [{
        id: Meteor.user().services.facebook.id,
        title: Meteor.user().profile.name,
        src: "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/"
      }]);
      return 'myschedule'
  },
  '/privatesetting': 'privatesetting',
  '/schedule/:id': {as: 'friendSchedule', to:function (id){      
      if (Meteor.loggingIn()) {
        return 'loading';
      } else if (Meteor.user()) {
        var user = Meteor.users.findOne({"services.facebook.id": id});
        Session.set("selectedFriends", [{
          id: user.services.facebook.id,
          title: user.profile.name,
          src: "http://graph.facebook.com/" + user.services.facebook.id + "/picture/"
        }]);
        return 'friendSchedule';
      } else {
        return 'landingPage';
      }
      
    }},
    '/': 'newHome',
  '*': 'landingPage'
});
Meteor.Router.filters({
  'checkLoggedIn': function(page) {
    if (Meteor.loggingIn()) {
      return 'loading';
    } else if (Meteor.user()) {
      setUser();
      if (Schedules.find({userId: Meteor.user().services.facebook.id}).count() < 1) {
        // Schedules.insert({
        //   dates: false,
        //   recurring: true,
        //   week: false,
        //   userId: Meteor.user().services.facebook.id,
        //   days: {
        //     Mon: {
        //       blocks: [],
        //       inWeek: 1
        //     },
        //     Tue: {
        //       blocks: [],
        //       inWeek: 2
        //     },
        //     Wed: {
        //       blocks: [],
        //       inWeek: 3
        //     },
        //     Thu: {
        //       blocks: [],
        //       inWeek: 4
        //     },
        //     Fri: {
        //       blocks: [],
        //       inWeek: 5
        //     },
        //     Sat: {
        //       blocks: [],
        //       inWeek: 6
        //     }
        //   }
        // });
        Meteor.call("insertSchedule");
      }
      return page;
    } else {
      return 'landingPage';
    }
  }
});
Meteor.Router.filter('checkLoggedIn', {except: 'friendSchedule'});

