//The Time constructor(similar to classes in classical OOP, but not quite).
//It creates an instance of time in a {minutes:x, hour:y} format.
//It has an equals function to compare to instances of Time.
Date.prototype.format = function (e) {
    var t = "";
    var n = Date.replaceChars;
    for (var r = 0; r < e.length; r++) {
        var i = e.charAt(r);
        if (r - 1 >= 0 && e.charAt(r - 1) == "\\") {
            t += i;
        } else if (n[i]) {
            t += n[i].call(this);
        } else if (i != "\\") {
            t += i;
        }
    }
    return t;
};
Date.replaceChars = {
    shortMonths: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
    longMonths: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
    shortDays: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
    longDays: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
    d: function () {
        return (this.getDate() < 10 ? "0" : "") + this.getDate();
    },
    D: function () {
        return Date.replaceChars.shortDays[this.getDay()];
    },
    j: function () {
        return this.getDate();
    },
    l: function () {
        return Date.replaceChars.longDays[this.getDay()];
    },
    N: function () {
        return this.getDay() + 1;
    },
    S: function () {
        return this.getDate() % 10 == 1 && this.getDate() != 11 ? "st" : this.getDate() % 10 == 2 && this.getDate() != 12 ? "nd" : this.getDate() % 10 == 3 && this.getDate() != 13 ? "rd" : "th";
    },
    w: function () {
        return this.getDay();
    },
    z: function () {
        var e = new Date(this.getFullYear(), 0, 1);
        return Math.ceil((this - e) / 864e5);
    },
    W: function () {
        var e = new Date(this.getFullYear(), 0, 1);
        return Math.ceil(((this - e) / 864e5 + e.getDay() + 1) / 7);
    },
    F: function () {
        return Date.replaceChars.longMonths[this.getMonth()];
    },
    m: function () {
        return (this.getMonth() < 9 ? "0" : "") + (this.getMonth() + 1);
    },
    M: function () {
        return Date.replaceChars.shortMonths[this.getMonth()];
    },
    n: function () {
        return this.getMonth() + 1;
    },
    t: function () {
        var e = new Date();
        return (new Date(e.getFullYear(), e.getMonth(), 0)).getDate();
    },
    L: function () {
        var e = this.getFullYear();
        return e % 400 === 0 || e % 100 !== 0 && e % 4 === 0;
    },
    o: function () {
        var e = new Date(this.valueOf());
        e.setDate(e.getDate() - (this.getDay() + 6) % 7 + 3);
        return e.getFullYear();
    },
    Y: function () {
        return this.getFullYear();
    },
    y: function () {
        return ("" + this.getFullYear()).substr(2);
    },
    a: function () {
        return this.getHours() < 12 ? "am" : "pm";
    },
    A: function () {
        return this.getHours() < 12 ? "AM" : "PM";
    },
    B: function () {
        return Math.floor(((this.getUTCHours() + 1) % 24 + this.getUTCMinutes() / 60 + this.getUTCSeconds() / 3600) * 1e3 / 24);
    },
    g: function () {
        return this.getHours() % 12 || 12;
    },
    G: function () {
        return this.getHours();
    },
    h: function () {
        return ((this.getHours() % 12 || 12) < 10 ? "0" : "") + (this.getHours() % 12 || 12);
    },
    H: function () {
        return (this.getHours() < 10 ? "0" : "") + this.getHours();
    },
    i: function () {
        return (this.getMinutes() < 10 ? "0" : "") + this.getMinutes();
    },
    s: function () {
        return (this.getSeconds() < 10 ? "0" : "") + this.getSeconds();
    },
    u: function () {
        var e = this.getMilliseconds();
        return (e < 10 ? "00" : e < 100 ? "0" : "") + e;
    },
    e: function () {
        return "Not Yet Supported";
    },
    I: function () {
        var e = null;
        for (var t = 0; t < 12; ++t) {
            var n = new Date(this.getFullYear(), t, 1);
            var r = n.getTimezoneOffset();
            if (e === null) e = r;
            else if (r < e) {
                e = r;
                break;
            } else if (r > e) break;
        }
        return this.getTimezoneOffset() == e | 0;
    },
    O: function () {
        return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + "00";
    },
    P: function () {
        return (-this.getTimezoneOffset() < 0 ? "-" : "+") + (Math.abs(this.getTimezoneOffset() / 60) < 10 ? "0" : "") + Math.abs(this.getTimezoneOffset() / 60) + ":00";
    },
    T: function () {
        var e = this.getMonth();
        this.setMonth(0);
        var t = this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
        this.setMonth(e);
        return t;
    },
    Z: function () {
        return -this.getTimezoneOffset() * 60;
    },
    c: function () {
        return this.format("Y-m-d\\TH:i:sP");
    },
    r: function () {
        return this.toString();
    },
    U: function () {
        return this.getTime() / 1e3;
    }
};

// GA = function() {
//   // var _gaq = _gaq || [];
//   // _gaq.push(['_setAccount', 'UA-43619080-1'])
//   // _gaq.push(['_setDomainName', 'findtime.co']);  
//   // _gaq.push(['_trackPageview', Meteor.Router.page()]);

//   // (function() {
//   //   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
//   //   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
//   //   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
//   // })();


// };


Time = function Time(str) {
  if (str.indexOf(":") === -1){
    var time = parseInt(str, 10);
    this.minutes = time % 100;
    this.hour = (time - this.minutes) / 100;
  }else {
    this.hour = parseInt(str.substring(0, str.indexOf(":")), 10);
    this.minutes = parseInt(str.substring(str.indexOf(":") + 1), 10);
  }
  if (this.hour < 7 ) {
    this.hour += 12;
  }
  return this;
};
Time.prototype.wrong = function () {
    return this.serialize() > 24 && 0 > this.serialize();
};
Time.parse = function (num) {
    var deci = (num % 1),
        minutes = deci * 60,
        hour = num >= 24 ? ((num - deci - 12) * 100) : ((num - deci) * 100), 
        number = hour + minutes;
    return new Time(number.toString());
};
Time.prototype.toString = function () {
    var minute;
    if (this.minutes >= 9) {
        minute = this.minutes.toString();
    } else {
        minute = "0" + this.minutes.toString();
    }
    return this.hour.toString() + ":" + minute;
};
Time.prototype.toAPString = function () {
    var minute;
    if (this.minutes > 9) {
        minute = this.minutes.toString();
    } else {
        minute = "0" + this.minutes.toString();
    }
  if (this.hour % 12 === 0 ) {
    return 12; 
  } else {
    return this.hour % 12;
  }
    return this.hour.toString() + ":" + minute;
};
Time.prototype.equals = function (time) {
    return this.minutes === time.minutes && this.hour === time.hour;
};
Time.prototype.serialize = function () {
    return this.hour + (this.minutes / 60);
};
Time.prototype.add = function() {
    var t = new Time(this.toString());
    if (t.minutes < 50){
      t.minutes += 10;
      t.hour = this.hour;
    } else {
      t.minutes = 0;
      t.hour = this.hour + 1;
    }
    return t;
};
Time.prototype.addMore = function (times) {
  var oldtime = Time.parse(this.serialize());
  for (i=1; i <= times - 1; i++ ) {
    oldtime = oldtime.add();
  }
  return oldtime;
};
Time.prototype.toMeridianString = function () {
    var minute,
        meridian = this.hour > 12 ? "PM" : "AM",
        hour;
    if (this.minutes > 9) {
        minute = this.minutes.toString();
    } else {
        minute = "0" + this.minutes.toString();
    }

  if (this.hour % 12 === 0 ) {
    hour = 12;
  } else {
    hour = this.hour % 12;
  }
    return hour.toString() + ":" + minute + " " + meridian;
};

//TimeRange is basically a time that goes from Time(x) to Time(y)
//It has methods like within which checks if the instance of Time passed is within the TimeRange.
TimeRange = function TimeRange() {
    if (typeof arguments[0] === 'string') {
        var range = arguments[0].split("-");
        this.start = new Time(range[0]);
        this.end = new Time(range[1]);
    } else {
        this.start = arguments[0];
        this.end = arguments[1];
    }

};
TimeRange.prototype.toString = function () {
    return this.start.toString() + "-" + this.end.toString();
};
TimeRange.prototype.addable = function (range) {
    return this.end.equals(range.start);
};
TimeRange.prototype.add = function (range) {
    return new TimeRange(this.start, range.end);
};
TimeRange.prototype.within = function (time) {
    var ref = time.serialize();
    return this.start.serialize() <= ref && ref <= this.end.serialize();
};
TimeRange.prototype.withinRange = function (range) {
    return this.within(range.start) && this.within(range.end);
};
TimeRange.prototype.overlappable = function (range) {
    return this.within(range.start) || range.within(this.start);
};
TimeRange.prototype.overlap = function (range) {
    return new TimeRange(Time.parse(Math.max(this.start.serialize(), range.start.serialize())), Time.parse(Math.min(this.end.serialize(), range.end.serialize())));
};
TimeRange.prototype.equals = function (range) {
    return range.start.equals(this.start) && range.end.equals(this.end);
};
TimeRange.prototype.serialize = function () {
    return [this.start.serialize(), this.end.serialize()];
};
TimeRange.prototype.length = function () {
    return (this.end.serialize() - this.start.serialize()) / 5;
};
TimeRange.prototype.cells = function () {
    var start = this.start,
        end = this.end,
        sHour = start.hour,
        sMinutes = start.minutes,
        eHour = end.hour,
        eMinutes = end.minutes;
    if (eMinutes < sMinutes) {
        eHour -= 1;
        eMinutes += 60;
    }
    return ((eHour - sHour) * 12) + ((eMinutes - sMinutes) / 5);
};
TimeRange.parse = function (arr) {
    return new TimeRange(Time.parse(arr[0]), Time.parse(arr[1]));
};
TimeRange.prototype.wrong = function () {
    return this.start.wrong() && this.end.wrong();
};

//Timeblock has a course a section and time. I will have to replace section with location and course with event, but that's it for now.
//Time in TimeBlock is actually a TimeRange. Sorry for any confusion.
TimeBlock = function TimeBlock(option) {
    this.course = option.course || "Break";
    this.section = option.section || "Break";
    this.time = option.time;
    this.priority = option.priority || "Meeting";
};
TimeBlock.prototype.sameCourse = function (block) {
    return this.course === block.course;
};
TimeBlock.prototype.sameSectionAndTime = function (block) {
    return this.section === block.section && this.time.equals(block.time);
};
TimeBlock.prototype.overlap = function (block) {
    return new TimeBlock({
        time: this.time.overlap(block.time)
    });
};
TimeBlock.prototype.overlappable = function (block) {
    var a = this.time,
        b = block.time;
    return a.overlappable(b);
};
TimeBlock.prototype.addTime = function (block) {
    return new TimeBlock({
        time: this.time.add(block.time),
        section: this.section,
        course: this.course,
        priority: this.priority
    });
};
TimeBlock.prototype.addableTime = function (block) {
    return this.time.addable(block.time);
};
TimeBlock.prototype.equals = function (block) {
    return this.time.equals(block.time) && this.section === block.section && this.course === block.course;
};
TimeBlock.prototype.serialize = function () {
    return {
        course: this.course,
        section: this.section,
        time: this.time.serialize(),
        priority: this.priority
    };
};
TimeBlock.parse = function (obj) {
    return new TimeBlock({
        course: obj.course,
        section: obj.section,
        time: TimeRange.parse(obj.time),
        priority: obj.priority
    });
};
TimeBlock.prototype.wrongTime = function () {
    return this.time.wrong();
};
TimeBlock.prototype.edit = function (obj) {
    _.extend(this, obj);
    return this;
};

Day = function Day(inWeek, userId) {
    this.blocks = [];
    this.inWeek = inWeek || 0;
    
};
Day.prototype.push = function (block) {
    this.blocks.push(block);
};
Day.prototype.sameSection = function (day) {
    var same = new Day();
    _.each(this.blocks, function (el, i) {
        _.each(day.blocks, function (element, index) {
            //console.log(el);
            if (el.sameSectionAndTime(element)) {
                same.push(el);
            }
        });
    });
    return same;
};
Day.prototype.overlappingBreak = function (day) {
    var a = this.breaks().blocks,
        b = day.breaks().blocks,
        breaks = new Day();
    _.each(a, function (el, i) {
        _.each(b, function (element, index) {
            if (el.overlappable(element)) {
                breaks.push(el.overlap(element));
            }
        });
    });
    return breaks;
};
Day.prototype.overlapAll = function (day) {
    var breaks = this.overlappingBreak(day),
        sameSection = this.sameSection(day),
        sameAll = new Day();
    this.blocks = breaks.blocks.concat(sameSection.blocks);
    this.sort();
    return sameAll.overwriteAll(this.blocks);
};
Day.prototype.sort = function () {
    this.blocks = _.sortBy(this.blocks, function (block) {
        return block.time.start.serialize();
    });
    return this;
};
Day.prototype.overwrite = function (block) {
    this.blocks.splice(this.blocks.length - 1, 1, block);
};
Day.prototype.overwriteAll = function (blocks) {
    this.blocks = blocks;
    return this;
};
Day.prototype.addBlock = function (block) {
    var last = _.last(this.blocks);
    if (last && last.addableTime(block)) {
        if (last.sameCourse(block)) {
            this.overwrite(last.addTime(block));
        } else {
            this.push(block);
        }
    } else {
        //this.push(new TimeBlock({
        //    time: new TimeRange(last.time.end, block.time.start)
        //}));
        this.push(block);
    }
};
Day.prototype.isEmpty = function () {
    return _.isEmpty(this.blocks);
};
Day.prototype.unshift = function (block) {
    this.blocks.unshift(block);
};
Day.prototype.find = function (block) {
    return _.find(this.blocks, function (bl){
        return block.course === bl.course && block.section === bl.section;
    })
};
Day.prototype.edit = function (block, newBlock) {
    var except = _.reject(this.blocks, function (bl){
        return bl.equals(block);
    });
    except.push(newBlock);
    this.overwriteAll(except);
    this.resolveConflict();
    return this;
}
Day.prototype.serialize = function () {
    return {
        blocks: _.map(this.blocks, function (tb) {
            return tb.serialize();
        }),
        inWeek: this.inWeek
    };
};
Day.parse = function (arr) {
    var day = new Day();
    day.overwriteAll(_.map(arr.blocks, function (obj) {
        return TimeBlock.parse(obj);
    }));
    day.inWeek = arr.inWeek;
    return day;
};
Day.prototype.wrongTime = function () {
    _.reduce(this.blocks, function (memo, block) {
        return memo && block.wrongTime();
    }, true);
};
/*Day.prototype.resolve = function () {
    var that = this,
        lastTime,
        a = [];
    if (this.blocks.length > 0) {
        this.unshift(new TimeBlock({
            time: new TimeRange(Time.parse(0), this.blocks[0].time.start)
        }));
        lastTime = this.blocks[0].time.end;
        _.each(this.blocks, function (block) {
            if (!block.time.start.equals(lastTime)) {
                a.push(new TimeBlock({
                    time: new TimeRange(lastTime, block.time.start)
                }));
            } else {
                a.push(block);
            }
            lastTime = block.time.end;
        });
        this.overwriteAll(a);
        console.log(this.serialize());
        this.push(new TimeBlock({
            time: new TimeRange(lastTime, new Time(2359))
        }));
    } else {
        this.push(new TimeBlock({
            time: new TimeRange(Time.parse(0), new Time("2359"))
        }));
    }
};*/
Day.prototype.stripBreaks = function () {
    var day = new Day();
    day.overwriteAll(_.filter(this.blocks, function (block) {
        return block.course !== "Break";
    }));
    return day;
};
Day.prototype.combine = function (day) {
    //var out = _.filter(this.blocks, function(block){
    //    return block.class === "Break";
    //}),
    //    noBreaks = _.filter(day.blocks, function(block){
    //    return block.class === "Break";
    //});
    var out = new Day();
    out.overwriteAll(this.stripBreaks.blocks.concat(day.stripBreaks.blocks));
    return out.sort();
};
Day.prototype.breaks = function () {
    var day = new Day(),
        lastTime = Time.parse(7);
    _.each(this.stripBreaks().blocks, function (el, i) {
        if (!lastTime.equals(el.time.start)) {
            day.push(new TimeBlock({
                time: new TimeRange(lastTime, el.time.start)
            }));
        }
        lastTime = el.time.end;
    });
    day.push(new TimeBlock({
        time: new TimeRange(lastTime, new Time("2100"))
    }));
    return day;
};
Day.prototype.combineWithBreaks = function (day) {
    var cons = this.combine(day),
        breaks = cons.breaks(),
        out = new Day();
    return out.overwriteAll(cons.blocks.concat(breaks.blocks)).sort();
};
Day.prototype.resolveConflict = function () {    
    var day = Day.parse(this.serialize());
    day.sort();
    var lastTime = day.blocks[0],
        day2 = Day.parse(this.serialize());    
    day2.blocks = [lastTime];
    _.each(_.rest(day.blocks), function(el) {        
        if (el.overlappable(lastTime)) {
            var temp1, temp2, ts1, ts2, te1, te2;
            temp1 = lastTime;
            temp2 = el;
            ts1 = temp1.time.start.serialize();
            ts2 = temp2.time.start.serialize();
            te1 = temp1.time.end.serialize();
            te2 = temp2.time.end.serialize();
            if (ts2 <= ts1){                
                if (te2 >= te1) {                    
                    day2.overwrite(temp2);
                } else {
                    day2.overwrite(temp2);
                    temp1.time.start = Time.parse(te2);
                    day2.push(temp1);
                }
            } else {
                if (te2 >= te1){
                    day2.overwrite(temp1);
                    temp2.time.start = Time.parse(te1);
                    day2.push(temp2);
                } else {
                    day2.overwrite(temp1);
                }
            }
        } else {
            day2.push(el);
        }
        lastTime = el;
    });
    return day2;
};

Days = function Days(rec, date) {
    var date = new Date(date);
    this.days = {};
    this.recurring = rec || false;
    this.userId = "";
    if (rec) {
        this.dates = false;
        this.week = false;
    } else {
        this.dates = [];
        for (i = 1; i < date.getDay(); i++) {
            this.dates.push(new Date(date - 1000 * 24 * 60 * 60 * (date.getDay() - i)));
        }
        for (i = date.getDay(); i <= 6; i++) {
            this.dates.push(new Date(date - 1000 * 24 * 60 * 60 * (6 - date.getDay())));
        }

        var d = date;
        d.setHours(0, 0, 0);
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        var yearStart = new Date(d.getFullYear(), 0, 1);
        this.week = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    }
};
Days.prototype.overwriteAll = function (days) {
    this.days = days;
    return this;
};
Days.prototype.overwrite = function (day, str) {
    this.days[str] = day;
    return this;
};
Days.prototype.serialize = function () {
    var obj = {
        days: {},
        recurring: this.recurring,
        dates: this.dates,
        week: this.week,
      userId: this.userId
    },
    self = this;
    _.each("Mon Tue Wed Thu Fri Sat".split(" "), function (day) {
        obj.days[day] = self.days[day].serialize();
    });
    return obj;
};
Days.prototype.add = function (day, block) {
  var today = this.days[day];
  today.addBlock(block);  
  this.overwrite(today.resolveConflict(), day);
  return this;
};
Days.prototype.find = function (day, block) {
    return this.days[day].find(block);
};
Days.prototype.edit = function(day, block, newBlock) {
    var d = this.days[day],
        store = d.find(block);
    d.edit(store, newBlock);
    this.overwrite(d.resolveConflict(), day)
    return this;
};
Days.prototype.remove = function(day, block) {
    var today = this.days[day],
        blocks = today.blocks,
        rejected = _.reject(today.blocks, function(b) {
            return b.course === block.course && b.section === block.section;
        });
  today.overwriteAll(rejected);
  this.overwrite(today, this.days[day]);
  return this;
};
Days.parse = function (obj) {
    var days = obj.recurring ? new Days(obj.recurring) : new Days(obj.recurring, obj.dates[6]),
        mydays = {};
    _.each(obj.days, function (day, k) {
        var dlist = "Mon Tue Wed Thu Fri Sat".split(" ");
        if (_.contains(dlist, k)){
            mydays[k] = Day.parse(day);
        }
    });
    days.userId = obj.userId;
    return days.overwriteAll(mydays);
};
reformat = function(input) {
  var out = [];
  for(i=7; i<=20;i++){
    for(j=0; j<=3; j++){
      var t = Time.parse(i),
          t2 = Time.parse(i);
      t.minutes = j * 15;
      t2.minutes = (j+1) * 15;
      if (t2.minutes >= 60){
        t2 = Time.parse(i+1);
      }
      out.push({
        time: new TimeRange(t, t2),
        people: {
          "Sun":[],
          "Mon":[],
          "Tue":[],
          "Wed":[],
          "Thu":[],
          "Fri":[],
          "Sat":[]
        }
      });
    }
  }
  //console.log(out);
  _.each(input, function(days) {
    var days = Days.parse(days);
    _.each(days.days, function(day, key){
      _.each(day.blocks, function(block){
        _.each(out, function(bl){ 
          if(block.time.withinRange(bl.time)){
            bl.people[key].push({
              course: block.course,
              section: block.section,
              userId: days.userId
            });     
          }
        });
      });
    });
  });
  //console.log(out);
  return _.groupBy(out, function(block){
    return block.time.start.hour;
  });
  // _.each(foo, function(hour){
  //   _(hour).groupBy( function(value) {
  //     return block.time.start.minutes;
  //   })
  // })
};
format = function(input) {
  var tempCourses = [];
  var out = {
    courses: {},
    times: [],
    peeps: []
  };
  for(i=7; i<=20;i++){
    for(j=0; j<=5; j++){
      var t = Time.parse(i),
          t2 = Time.parse(i),
          trange;
      t.minutes = j * 10;
      t2.minutes = (j+1) * 10;
      if (t2.minutes >= 60){
        t2 = Time.parse(i+1);
      }
      trange = new TimeRange(t, t2);
      out.times.push(trange);
      out.peeps.push({
        people: {
          "Sun":{span:1, profiles: [], time: trange.toString(), day: "Sunday"},
          "Mon":{span:1, profiles: [], time: trange.toString(), day: "Monday"},
          "Tue":{span:1, profiles: [], time: trange.toString(), day: "Tuesday"},
          "Wed":{span:1, profiles: [], time: trange.toString(), day: "Wednesday"},
          "Thu":{span:1, profiles: [], time: trange.toString(), day: "Thursday"},
          "Fri":{span:1, profiles: [], time: trange.toString(), day: "Friday"},
          "Sat":{span:1, profiles: [], time: trange.toString(), day: "Saturday"}
        }
      });
    }
  }
  //console.log(out); 
  _.each(input, function(days) {
    var days = Days.parse(days);
    _.each(days.days, function(day, key){
      _.each(day.blocks, function(block){
        _.each(out.times, function(bl, index){ 
          if(block.time.withinRange(bl)){
            tempCourses.push(block.course);            
            out.peeps[index].people[key].profiles.push({
              course: block.course,
              section: block.section,
              userId: days.userId
            });     
          }
        });
      });
    });
  });
  //console.log(out);
  out.times = _.map(out.times, function (time) {
    return time.toString();
  });
  _.each(["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], function(d) {
    _.each(out.peeps, function(people, i) {
      var ppl = people.people[d], int;
      int = i + 1;
      if (!_.isEmpty(ppl.profiles) && (int < out.times.length)) {
        while (_.isEqual(out.peeps[int].people[d].profiles, ppl.profiles) && int < out.times.length && out.peeps[int].people[d].span > 0) {
               ppl.span += 1;
               out.peeps[int].people[d].span = 0;
               int += 1;
        }
             }
    });
  });
  _.each(_.uniq(tempCourses), function (course) {
    out.courses[course] = 'rgb(' + (Math.floor((256 - 149) * Math.random()) + 150) + ',' + (Math.floor((256 - 149) * Math.random()) + 150) + ',' + (Math.floor((256 - 149) * Math.random()) + 150) + ')';
  });
  
  return out;
  // _.each(foo, function(hour){
  //   _(hour).groupBy( function(value) {
  //     return block.time.start.minutes;
  //   })
  // })
};
FQLQuery = function(q, session) {
    if (Session.get("FBLoaded")) {
        FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
         //Logic here             
            Session.set(session, response);
        });                 
    }
};
getEvents = function () {
    var ids = _.pluck(Session.get("selectedFriends"), "id");
    if (Meteor.Router.page() === "friendSchedule"){
        ids = _.reject(ids, function (id){
            return id === Meteor.user().services.facebook.id;
        });
    }
    var out = {vent: [], courses: {}};
  _.each(ids, function(id){
    
    var query = "SELECT name, start_time, end_time, location FROM event WHERE eid IN (SELECT eid FROM event_member WHERE uid = " + id + ") AND start_time >= now() and start_time <= (now() + (7 * 1000 * 60 * 60 * 24))";
    var getResponse = function () {  
        var dfd = new $.Deferred();      
        if (Session.get("FBLoaded")) {
            
            FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here             
                if (response.data)
                    dfd.resolve(response.data);
            });                 
            
        }    
        return dfd.promise();
    };
    var ven = getResponse().done(function (vents) {
         _.each(vents, function(ev){
            var start = new Date(ev.start_time),
                end = ev.end_time ? new Date(ev.end_time) : new Date(start + (1000 * 60 * 60)),
                time = new TimeRange(start.toTimeString().substring(0,5) + "-" + end.toTimeString().substring(0,5)),
                dayNum = start.getDay(),
                daylist = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                key = daylist[dayNum],
                times = ["7:00-7:10", "7:10-7:20", "7:20-7:30", "7:30-7:40", "7:40-7:50", "7:50-8:00", "8:00-8:10", "8:10-8:20", "8:20-8:30", "8:30-8:40", "8:40-8:50", "8:50-9:00", "9:00-9:10", "9:10-9:20", "9:20-9:30", "9:30-9:40", "9:40-9:50", "9:50-10:00", "10:00-10:10", "10:10-10:20", "10:20-10:30", "10:30-10:40", "10:40-10:50", "10:50-11:00", "11:00-11:10", "11:10-11:20", "11:20-11:30", "11:30-11:40", "11:40-11:50", "11:50-12:00", "12:00-12:10", "12:10-12:20", "12:20-12:30", "12:30-12:40", "12:40-12:50", "12:50-13:00", "13:00-13:10", "13:10-13:20", "13:20-13:30", "13:30-13:40", "13:40-13:50", "13:50-14:00", "14:00-14:10", "14:10-14:20", "14:20-14:30", "14:30-14:40", "14:40-14:50", "14:50-15:00", "15:00-15:10", "15:10-15:20", "15:20-15:30", "15:30-15:40", "15:40-15:50", "15:50-16:00", "16:00-16:10", "16:10-16:20", "16:20-16:30", "16:30-16:40", "16:40-16:50", "16:50-17:00", "17:00-17:10", "17:10-17:20", "17:20-17:30", "17:30-17:40", "17:40-17:50", "17:50-18:00", "18:00-18:10", "18:10-18:20", "18:20-18:30", "18:30-18:40", "18:40-18:50", "18:50-19:00", "19:00-19:10", "19:10-19:20", "19:20-19:30", "19:30-19:40", "19:40-19:50", "19:50-20:00", "20:00-20:10", "20:10-20:20", "20:20-20:30", "20:30-20:40", "20:40-20:50", "20:50-21:00"];
            _.each(times, function(bl, index){                       
                if (time.withinRange(new TimeRange(bl))){                    
                    // console.log(ev);
                    // console.log(bl);
                    out.courses[ev.name] = 'rgb(' + (Math.floor((256 - 149) * Math.random()) + 150) + ',' + (Math.floor((256 - 149) * Math.random()) + 150) + ',' + (Math.floor((256 - 149) * Math.random()) + 150) + ')';
                    out.vent.push({
                      block:{
                          course: ev.name,
                          section: ev.location,
                          userId: id
                      },
                      index: index,
                      key: key
                    });  
                    var temp = out.vent;                    
                }
            });
        });
        if (!_.isEqual(Session.get("prev"), Session.get("selectedFriends"))) {
            Session.set("FBEvents", out);
            Session.set("prev", Session.get("selectedFriends"));
        }
        
    });
    
   
  });  
  
};
merge = function (sched, fbevents) {
    var out = sched;
    _.each(fbevents.vent, function (vent) {
        out.peeps[vent.index].people[vent.key].profiles.push(vent.block);    
    });
     _.each(["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], function(d) {
    _.each(out.peeps, function(people, i) {
      var ppl = people.people[d], int;
      int = i + 1;
      if (!_.isEmpty(ppl.profiles) && (int < out.times.length)) {
        while (_.isEqual(out.peeps[int].people[d].profiles, ppl.profiles) && int < out.times.length && out.peeps[int].people[d].span > 0) {
               ppl.span += 1;
               out.peeps[int].people[d].span = 0;
               int += 1;
        }
             }
    });
  });
    return out;
};

Handlebars.registerHelper("key_value", function (obj, fn) {
    var buffer = "",
        key;

    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            buffer += fn({
                key: key,
                value: obj[key]
            });
        }
    }

    return buffer;
});
Handlebars.registerHelper('restOfList', function (context, block) {
    var ret = "",
        offset = parseInt(block.hash.offset, 10) || 1,
        limit = parseInt(block.hash.limit, 10) || 20,
        i = (offset < context.length) ? offset : 0,
        j = ((limit + offset) < context.length) ? (limit + offset) : context.length;

    for (i, j; i < j; i++) {
        ret += block(context[i]);
    }

    return ret;
});
Handlebars.registerHelper('firstInList', function (context, block) {
    return block(context[0]);
});

// Session.set("currentSchedules", [ , {
//     days: {
//         Mon: {
//             blocks: [{
//                 course: "EN 11",
//                 section: "R27 B-306",
//                 time: [9.5, 10.5]
//             }, {
//                 course: "LIT 13",
//                 section: "R27 B-306",
//                 time: [10.5, 11.5]
//             }, {
//                 course: "MA 18B",
//                 section: "K SEC-A124A",
//                 time: [12.5, 13.5]
//             }, {
//                 course: "CS 21A",
//                 section: "E F-227",
//                 time: [13.5, 14.5]
//             }],
//             inWeek: 1
//         },
//         Tue: {
//             blocks: [{
//                 course: "MA 18B",
//                 section: "K CTC 306",
//                 time: [10.5, 12]
//             }, {
//                 course: "PE 125",
//                 section: "N(MEN) COV COURTS",
//                 time: [13, 14]
//             }, {
//                 course: "FIL 11",
//                 section: "RR B-306",
//                 time: [15, 16.5]
//             }],
//             inWeek: 2
//         },
//         Wed: {
//             blocks: [{
//                 course: "EN 11",
//                 section: "R27 B-306",
//                 time: [9.5, 10.5]
//             }, {
//                 course: "LIT 13",
//                 section: "R27 B-306",
//                 time: [10.5, 11.5]
//             }, {
//                 course: "MA 18B",
//                 section: "K SEC-A124A",
//                 time: [12.5, 13.5]
//             }, {
//                 course: "CS 21A",
//                 section: "E F-227",
//                 time: [13.5, 14.5]
//             }, {
//                 course: "INTAC 1",
//                 section: "NN G-205A",
//                 time: [14.5, 15.5]
//             }],
//             inWeek: 3
//         },
//         Thu: {
//             blocks: [{
//                 course: "MA 18B",
//                 section: "K CTC 306",
//                 time: [10.5, 12]
//             }, {
//                 course: "PE 125",
//                 section: "N(MEN) COV COURTS",
//                 time: [13, 14]
//             }, {
//                 course: "FIL 11",
//                 section: "RR B-306",
//                 time: [15, 16.5]
//             }],
//             inWeek: 4
//         },
//         Fri: {
//             blocks: [{
//                 course: "EN 11",
//                 section: "R27 B-306",
//                 time: [9.5, 10.5]
//             }, {
//                 course: "LIT 13",
//                 section: "R27 B-306",
//                 time: [10.5, 11.5]
//             }, {
//                 course: "MA 18B",
//                 section: "K SEC-A124A",
//                 time: [12.5, 13.5]
//             }, {
//                 course: "CS 21A",
//                 section: "E F-227",
//                 time: [13.5, 15.5]
//             }],
//             inWeek: 5
//         },
//         Sat: {
//             blocks: [],
//             inWeek: 6
//         }
//     },
//     recurring: true,
//     dates: false,
//     week: false,
//     userId: "1441662945"
// }]);
//Events are similar to backbone.js. 
//Keys represent the event and the target.
//The value represents the action to be done when event is triggered.
Template.body.created = function () {
  //   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  //     (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  //     m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  //     })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  // //production
  // ga('create', 'UA-43619080-1', 'findtime.co');
  
  //testing
  // ga('create', 'UA-43619080-1', {
  //     'cookieDomain': 'none'
  //   });
    Session.set("FBLoaded", false);
    $(document).ready(function() {
      $.ajaxSetup({ cache: true });
      $.getScript('//connect.facebook.net/en_UK/all.js', function(){
        FB.init({
          appId: '517570874963582',
          channelUrl: '//findtime.co/channel.html'
        });           
        Session.set("FBLoaded", true);          
      });
    });
};
Template.body.events({
    'click .login': function () {
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'login'          
        });
        Meteor.loginWithFacebook({
            requestPermissions: ['user_education_history', 'publish_actions', 'email', 'xmpp_login', 'user_events', "friends_events", 'user_groups', 'create_event', 'read_friendlists']
        }, function (err) {
            if (err) {
                bootbox.alert("Please allow us to check out your Facebook profile, so we can create events during common breaks and help you compare with your friends' schedules");
                // console.log(err);
                ga('send', {
                  'hitType': 'event',          // Required.
                  'eventCategory': 'button',   // Required.
                  'eventAction': 'click',      // Required.
                  'eventLabel': 'Failed Login'          
                });
            } else {
                if (Schedules.find({userId: Meteor.user().services.facebook.id}).count() > 0){
                    Meteor.Router.to('/');
                    ga('send', {
                      'hitType': 'event',          // Required.
                      'eventCategory': 'button',   // Required.
                      'eventAction': 'click',      // Required.
                      'eventLabel': 'Returning user'          
                    });
                } else {
                    Meteor.Router.to('/myschedule');
                    ga('send', {
                      'hitType': 'event',          // Required.
                      'eventCategory': 'button',   // Required.
                      'eventAction': 'click',      // Required.
                      'eventLabel': 'New user'          
                    });
                }                                 
            }
        });
    }
});
Template.navigation.events({
    'click #logout': function () {
        Meteor.logout();
        Meteor.Router.to('/');
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'logout'          
        });
    },  
  'click a': function(e) { 
    var $root = $('html, body');
    var href = $(e.target).attr("href");
    if (href !== "#"){
        $root.animate({
        scrollTop: $(href).offset().top -38
    }, 500, function () {
        window.location.hash = href;
    });
      return false;
    }
  }
});
Template.navigation.helpers({
    currentUsername: function () {
        return Meteor.user().profile.name;
    }
});



//Created is an event when the template is first rendered.
//This only happens once in a template's lifecycle.
Template.landingPage.created = function () {
    var i = -1,
        list = ["your friends", "that cute girl", "your orgmates", "that cute guy","your Kada", "your crush", "your groupmates"];
    ga('send', 'pageview', {
        'page': "/" + Meteor.Router.page(),
        'title': Meteor.Router.page()
      });
    Meteor.setInterval(function () {
        if (i < 3) {
            i += 1;
        } else {
            i = 0;
        }
        Session.set("commonWith", list[i]);
    }, 2000);
};

Template.landingPage.helpers({
    commonWith: function () {
        return Session.get("commonWith") || "your friends";
    }  
});

Template.signup.events({
    'keypress #select-school': function (e) {
        Session.set("School", $(e.target).val());
    }
});
Template.signup.helpers({
    gender: function () {
        if (Meteor.user()) {
            switch (Meteor.user().services.facebook.gender) {
                case "male":
                    return "bro";
                case "female":
                    return "sis";
                case "undecided":
                    return "undecided";
                default:
                    return "man";
            }
        } else {
            return "dude";
        }
    }
});
Template.landingPage.rendered = function () {
    $('.navbar').css("margin-bottom", 0);
  
    var schedule = $("#schedule").height();
    var landing = $("#landing").height();
    //console.log(landing);
    $("#schedule").css("margin-top", ((landing - schedule) / 3  ));

};
Template.myschedule.rendered = function() {
    $(function(){
        var tour = new Tour({
                basePath: "/myschedule"
        });
        tour.addSteps([
            {
                element: "h1", // string (jQuery selector) - html element next to which the step popover should be shown
                title: "Welcome to FindTime.co", // string - title of the popover
                content: "Please input your class schedule<br>So that your friends know when you're busy.<br>Just click and drag on the blocks<br>In which you have classes or are busy.<br>It saves everything automatically,<br>So you don't have to worry:)<br>Note: Input your classes, and leave your breaks as a white space",                
                placement: "bottom"
            },
            {
                element: ".reset",
                title: "Reset",
                content: "Click reset when you want to clear your entire schedule.",
                placement: "bottom"
            },
            {
                element: ".done",
                title: "When you're done, click done",
                content: "Click done when you finish editing",                
                placement: "bottom"
            }
        ]);
        tour.start();
    });
};
Template.myschedule.helpers({
  fullname: function () {
    if (Meteor.user())
        return Meteor.user().profile.name;
    },
  email: function () {
        if (Meteor.user())
          return Meteor.user().services.facebook.email;
     }
});
Template.myschedule.events({
    'click .done': function() {
        Meteor.Router.to("/");
    },
    'click .reset': function () {
        var id = Schedules.findOne({userId: Meteor.user().services.facebook.id})._id;
        Meteor.call("updateSchedule", id, {
          dates: false,
          recurring: true,
          week: false,
          userId: Meteor.user().services.facebook.id,
          days: {
            Mon: {
              blocks: [],
              inWeek: 1
            },
            Tue: {
              blocks: [],
              inWeek: 2
            },
            Wed: {
              blocks: [],
              inWeek: 3
            },
            Thu: {
              blocks: [],
              inWeek: 4
            },
            Fri: {
              blocks: [],
              inWeek: 5
            },
            Sat: {
              blocks: [],
              inWeek: 6
            }
          }
        });
      }    
});
//schedule for settings page -> update database/currentSchedule
// Template.personalschedule.rendered = function() {
  
//   $('table').selectable({
//   filter:'td',
//   stop: function(event, ui){}
// });
//     var selectedTD;
//         $('table').fixedHeaderTable({
//         width: 700,
//         height: 400,
//         fixedColumns: 1,
//         classHeader: "fixedHead",
//         classFooter: "fixedFoot",
//         classColumn: "fixedColumn",
//         fixedColumnWidth: 80,
//         outerId: "sched",
//         backcolor: "#FFFFFF",
//         hovercolor: "#99CCFF"
//     });
  
//       var daylist = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//       var selection = function (selector, bgcolor) {
//       var $selected = $(selector),
//             fmin = $selected.parent().index() * 15,
//             hour = $selected.parent().parent().index() + 6,
//             ftime = new Time(hour + ":" + fmin),
//             timerange = new TimeRange(ftime, ftime.add());            
//         $selected.css({
//             "backgroundColor": bgcolor                
//         }).find('.forpopovers').css({"color": "#FFF"}).attr("data-eventtime", daylist[$selected.index()] + " " + timerange.toString());
//     };
//   //make each cell a time block
  

// };
// Template.personalschedule.helpers({
//     schedules: function () {
//         return reformat(Session.get("Schedules"));
//        // console.log(currentSchedules);
//     },
//     daylist: function () {
//         return ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//     },
//     stringify: function (obj) {
//         return JSON.stringify(obj);
//     }
  
 
// });

// Template.schedule.helpers({
//     schedules: function () {
//         return reformat(Session.get("Meteor.Collection.schedules"));
//     },
//     daylist: function () {
//         return ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//     },
//     stringify: function (obj) {
//         return JSON.stringify(obj);
//     },
//     getName: function (id) {
//         return new Handlebars.SafeString("<img class='profile32' src='http://graph.facebook.com/" + id + "/picture/' />  ");
//     },
//     getBGStyle: function (day) {
//         var len = Session.get("currentSchedules").length;
//         dlen = day.length,
//         rgbnum = Math.floor((255 * (len - dlen)) / len),
//         color = rgbnum < 170 ? "#FFF" : "#000";
//         return "background-color: rgb(" + rgbnum + ", " + rgbnum + ", " + rgbnum + ");";
//     },
//     getFontStyle: function (day) {
//         var len = Session.get("currentSchedules").length;
//         dlen = day.length,
//         rgbnum = Math.floor((255 * (len - dlen)) / len),
//         color = rgbnum < 170 ? "#FFF" : "#000";
//         return "color: " + color + ";";
//     },
//     getStyle: function (day) {
//         var len = Session.get("currentSchedules").length;
//         dlen = day.length,
//         rgbnum = Math.floor((255 * (len - dlen)) / len),
//         color = rgbnum < 170 ? "#FFF" : "#000";
//         return "background-color: rgb(" + rgbnum + ", " + rgbnum + ", " + rgbnum + "); color: " + color + ";";
//     }
// });
// Template.schedule.rendered = function () {
//     var selectedTD;
//     $('table').fixedTable({
//         width: 700,
//         height: 400,
//         fixedColumns: 1,
//         classHeader: "fixedHead",
//         classFooter: "fixedFoot",
//         classColumn: "fixedColumn",
//         fixedColumnWidth: 80,
//         outerId: "sched",
//         backcolor: "#FFFFFF",
//         hovercolor: "#99CCFF"
//     });
//     $('.fixedContainer').find('.fixedHead').width($('.fixedContainer').find('.fixedTable').width());
//     $('.fixedColumn').find('.fixedTable').height($('.fixedContainer').find('.fixedTable').height());
//     var daylist = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
//     var selection = function (selector, bgcolor) {
//         var $selected = $(selector),
//             fmin = $selected.parent().index() * 15,
//             hour = $selected.parent().parent().index() + 6,
//             ftime = new Time(hour + ":" + fmin),
//             timerange = new TimeRange(ftime, ftime.add());            
//         $selected.css({
//             "backgroundColor": bgcolor                
//         }).find('.forpopovers').css({"color": "#FFF"}).attr("data-eventtime", daylist[$selected.index()] + " " + timerange.toString());
//     };
//     var serialize = function (selectors) {
//         var split = _.map(selectors, function (el, i) {
//             return $(el).attr("data-eventtime").split(" ");
//         }),
//             grouped = _.groupBy(split, function (arr) {
//                 return arr[0];
//             }),
//             out = {};            
//         _.each(grouped, function (v, k) {
//             var output = [],
//                 vals = _.map(v, function (arr) {                    
//                     return new TimeRange(arr[1]);
//                 }),
//                 lastTime = _.first(vals);
//             if (vals.length > 1) {
//                 //vals.shift();                    
//                 _.each(vals, function (value) {
//                     if (lastTime.addable(value)) {
//                         lastTime = lastTime.add(value);
//                     } else {
//                         output.push(lastTime);
//                         lastTime = value;
//                     }
//                 });
//                 if (!lastTime.end.equals(_.last(output))) {
//                     output.push(lastTime);
//                 }
//             } else {
//                 output.push(lastTime);
//             }                        
//             out[k] = output;
//             if (output.length > 1)
//                 output.shift()
//         });
//         return out;
//     };
//     $('.fixedContainer').find('.fixedTable').selectableScroll({
//         filter: "td",
//         selected: function (ev, ui) {
//             selection(ui.selected, '#F39814');
//         },
//         selecting: function (ev, ui) {
//             var $uiselecting = $(ui.selecting),
//                 forpopovers = $uiselecting.find('.forpopovers').first();
//             if (!$uiselecting.attr("selected")) {
//                 $uiselecting.attr("selected", "true");
//                 $uiselecting.attr("data-oldstyle", $uiselecting.attr("style"));             
//                 forpopovers.attr("data-oldhtml", forpopovers.html());                               
//             }
//             selection(ui.selecting, '#99CCFF');

//         },
//         // unselecting: function (ev, ui) {
//         //     var $uiselecting = $(ui.unselecting);
//         //     selection(ui.unselecting, '#rgb(200, 100, 100)');
//         //     // $uiselecting.css({
//         //     //     "backgroundColor": '#FFF'
//         //     // }).text("");
//         //     // $uiselecting.attr("style", $uiselecting.attr("data-oldstyle"));
//         //     // $uiselecting.html($uiselecting.attr("data-oldhtml"));
//         //     // $uiselecting.attr("selected", "");
//         // },
//         unselected: function (ev, ui) {
//             var $uiselecting = $(ui.unselected),
//             forpopovers = $uiselecting.find('.forpopovers'),
//             oldhtml = forpopovers.attr("data-oldhtml");                   
//             forpopovers.html(oldhtml);
//             $uiselecting.css({
//                "backgroundColor": '#FFF'
//             });
//             $uiselecting.attr("style", $uiselecting.attr("data-oldstyle"));            
//             $uiselecting.attr("selected", "");
//         },
//         stop: function (ev, ui) {            
//             var $this = $(this),
//                 selectedTD = $this.find('.ui-selected'),                
//                 forpopovers = selectedTD.last().find('.forpopovers'),
//                 data = serialize(selectedTD.find('.forpopovers')), 
//                 keys = _.keys(data),                    
//                 len = keys.length,                
//                 spanSize = len > 3 ? Math.floor(24/len) : Math.floor(12/len),                                
//                 finalData = '',
//                 inputTmpl = function(v, txt) {
//                     var out = "" + txt;
//                     out += '<div class="input-append bootstrap-timepicker">';
//                     out +=  '<input type="text" class="input-small tpicker" data-default-time="' + v + '"/>';
//                     out +=  '<span class="add-on">';
//                     out +=     '<i class="icon-time"></i>';
//                     out +=  '</span>'
//                     out += '</div>'
//                     return out;
//                 };
//                 // finalData += '<form class="form-horizontal">';
//                 // finalData +=     '<div class="control-group">';
//                 // finalData +=        '<label class="control-label" for="inputEvent">Event</label>';
//                 // finalData +=        '<div class="controls">';
//                 // finalData +=           '<input type="text" id="inputEvent" placeholder="Event">';
//                 // finalData +=        '</div>';
//                 // finalData +=     '</div>';
//                 // finalData +=     '<div class="control-group">';
//                 // finalData +=        '<label class="control-label" for="inputLocation">Location</label>';
//                 // finalData +=        '<div class="controls">';
//                 // finalData +=          '<input type="password" id="inputLocation" placeholder="Location">';
//                 // finalData +=        '</div>';
//                 // finalData +=      '</div>';
//                 // finalData +=  '</form>';
//                 finalData += len % 3 === 0 ? '<form class="form-inline">' : '<form>';
//                 finalData += '<label>Event: </label><input type="text" placeholder="Eat with Friends" id="inputEvent">    ';
//                 finalData += '<label>Location: </label><input type="text" placeholder="JSEC" id="inputLocation">';
//                 finalData += '</form>';
//                 finalData += '<div class="container-fluid"><div class="row-fluid">';                                                                  
//                 _.each(data, function(v,k) {                      
//                     finalData += '<div class="span' + spanSize + '"';                    
//                     if ((len === 4 && keys.indexOf(k) === 2) || (len === 6 && keys.indexOf(k) === 3)) {
//                         finalData += ' style="margin-left:0;"'
//                     }                    
//                     finalData += '>' + "<h3>" + k + "</h3>";
//                     _.each(v, function(value){
//                         var time1 = value.start.toString(),
//                             time2 = value.end.toString();
//                         // finalData += '<p>Start:<input type="text" value="' + time1 + '" class="timepicker" /></p>';
//                         // finalData += '<p>End:<input type="text" value="' + time2 + '" class="timepicker" /></p>';    
//                         finalData += inputTmpl(time1, 'Start: ') + "<br>" + inputTmpl(time2, 'End : ');
//                     }); 
//                     finalData += "</div>"                   
//                 });                
//                 finalData += "</div></div><div class='pull-right' style='margin-bottom:5px;'><a href='#' class='btn btn-primary sendreq'>Send request</a>  <a class='btn btn-danger cancelreq'>Cancel</a></div><br>"
//                 forpopovers.attr("data-content", finalData).data('popover').setContent();
//                 $this.find('.forpopovers').popover('hide');
//                 forpopovers.popover('show');                 
//                 $('.tpicker').timepicker({
//                     minuteStep:1,
//                     showMeridian: false
//                 });               
//                 forpopovers.siblings('.popover').css('min-width', function (){
//                         if (len < 5 && len % 2 === 0){
//                             return 430;
//                         } else if(len > 2) {
//                             return 600;
//                         } else {
//                             return 250;
//                         }
//                 }).on('mousedown', '.sendreq', function(e) {
//                     e.stopImmediatePropagation();
//                     bootbox.alert("Sent request");
//                     forpopovers.popover('hide');
//                     var oldhtml = forpopovers.attr("data-oldhtml");                   
//                     forpopovers.html(oldhtml);                    
//                     selectedTD.css({
//                        "backgroundColor": '#FFF'
//                     });
//                     selectedTD.attr("style", selectedTD.attr("data-oldstyle"));                    
//                     selectedTD.attr("selected", "");
//                     selectedTD.removeClass('.ui-selected');
//                     return false;
//                 }).on('mousedown', '.cancelreq', function (e) {
//                     e.stopImmediatePropagation();
//                     forpopovers.popover('hide');
//                     var oldhtml = forpopovers.attr("data-oldhtml");                   
//                     forpopovers.html(oldhtml);                    
//                     selectedTD.css({
//                        "backgroundColor": '#FFF'
//                     });
//                     selectedTD.attr("style", selectedTD.attr("data-oldstyle"));                    
//                     selectedTD.attr("selected", "");
//                     selectedTD.removeClass('.ui-selected');
                    
//                     return false;
//                 }).on('mousedown', function (e){
//                     e.stopImmediatePropagation();
//                     return false;
//                 });
//         }
//     }).find('.forpopovers').popover({
//         html: 'true',
//         trigger: 'manual',
//         title: 'You found time time to set it up'
//     });    
// };
//The helpers below are filler for next time.

Template.peopleInSchedule.helpers({
    images: function () {
        return _.reject(Session.get("selectedFriends"), function (obj) {
            return obj.id === Meteor.user().services.facebook.id;
        });
    }
});
Template.peopleInSchedule.events({
    'click .clear-all': function () {
        Session.set("selectedFriends", [{
            id: Meteor.user().services.facebook.id,
            title: Meteor.user().profile.name,
            src: "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/"
          }]);
    },
    'click .compare': function (){
        $("#sidebar").toggle();
    },
    'click .help': function () {
        var tour = new Tour();
        tour.addSteps([
            {
                element: "h3:first",
                title: "Home",
                content: "Here you can click on your friends to compare schedules and set up meetings and hang outs.",
                placement: "bottom"
            },
            {
                element:'.isUser:first',
                title: "Compare with friends",
                content: "Click on your friend's schedule in order to see what he's up to",
                placement: "left",
                reflex: true
            },
            {
                element: ".profile-bottom:first",
                title: "When you're finished comparing",
                content: "Once you're done click on your friend's picture to take the person out of comparison",
                placement: "top"
            },
            {
                element: "a[href=#groups]",
                title: "Groups",
                content: "Your Facebook groups are automatically imported to FindTime. Click on See Group's Schedule to see common areas of free time for meetings\nNote: Members of the Facebook group must post their schedule on FindTime",
                placement: "left",
                onShow: function (tour){
                    $('a[href=#groups]').trigger('click');
                }
            },
            {
                element: ".clear-all",
                title: "Clear",
                content: "Removing each person out of comparison is a hassle, so here's a button to help you clear them all.",
                reflex: true,
                placement: "top"
            },
            {
                element: ".help:first",
                title: "Help",
                content: "If you ever get lost, click Help",
                reflex: true,
                placement: "top"
            }
        ]);
        tour.restart();
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'Help in Home'
        });
    }
});
Template.bottomNavImage.rendered = function () {
    $(this.firstNode).tooltip();
};
Template.bottomNavImage.events({
    click: function(e) {
        prev = Session.get("selectedFriends"),
        removed = _.reject(prev, function(obj) {
            return obj.id === $(e.target).parent().attr("data-id");
        });        
        Session.set("selectedFriends", removed);
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'remove friend from schedule',
          'eventValue': removed.length                                      
        });
    }
});
Template.peopleToAdd.helpers({
    profiles: function () {
        var arr = [],
        exclude = function (arr) {
            return _.reject(arr, function (obj) {
                var matching  = _.where(Session.get("selectedFriends"), {id: obj.id});
                return matching.length > 0;
            });
        };
        Meteor.call("findFriends", function(err, res) {
          Session.set("friends", res);
        });                    
          return _.isEmpty(Session.get("filteredFriends")) ? exclude(Session.get("friends")) : exclude(Session.get("filteredFriends")) || [{id:'goo', src:"http://www.henley-putnam.edu/Portals/_default/Skins/henley/images/loading.gif", name: "Loading"}];
    },
    groups: function () {
        var query = "select gid, name from group where gid in (select gid from group_member where uid = me() and bookmark_order < 16 )";        
        // Meteor.call("findGroups", function (err, res){
        //    Session.set("groups", res);
        // });
        if (Session.get("FBLoaded")) {
            FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here             
                Session.set("groups", response.data);
            });                 
        }
        return _.isEmpty(Session.get("filteredGroups")) ? Session.get("groups") : Session.get("filteredGroups");
    },
    profilesRegistered: function (){
         var arr = [],
        exclude = function (arr) {
            return _.reject(arr, function (obj) {
                var matching  = _.where(Session.get("selectedFriends"), {id: obj.id});
                return matching.length > 0;
            });
        };
        var query = "SELECT uid, name FROM user WHERE is_app_user AND uid IN (SELECT uid2 FROM friend WHERE uid1 = me())";
        // Meteor.call("findRealFriends", function(err, res) {
        //   Session.set("friends", res);
        // });   
        if (Session.get("FBLoaded")) {
            FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here
             arr = _.map(response.data, function(obj) {
                  return {
                      id: "" + obj.uid,
                      name: obj.name,
                      src: "http://graph.facebook.com/" + obj.uid + "/picture/"             
                  };
                });
                Session.set("friends", arr);
            });                 
        }
          return _.isEmpty(Session.get("filteredFriends")) ? exclude(Session.get("friends")) : exclude(Session.get("filteredFriends"));
    },
    profilesCityMates: function () {
         var arr = [],
        exclude = function (arr) {
            return _.reject(arr, function (obj) {
                var matching  = _.where(Session.get("selectedFriends"), {id: obj.id});
                return matching.length > 0;
            });
        };
        // Meteor.call("findCityMates", function(err, res) {
        //   Session.set("cityMates", res);
        // });        
        var query = "SELECT uid, name FROM user WHERE not(is_app_user) AND uid IN (SELECT uid FROM friendlist_member WHERE flid IN (select flid from friendlist WHERE owner = me() and type = 'current_city')) LIMIT 30";        
        if (Session.get("FBLoaded")) {
            FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here
             arr = _.map(response.data, function(obj) {
                  return {
                      id: "" + obj.uid,
                      name: obj.name,
                      src: "http://graph.facebook.com/" + obj.uid + "/picture/"              
                  };
                });
                Session.set("cityMates", arr);
            });                 
        }            
          return Session.get("cityMates");
    }
});
Template.peopleToAdd.rendered = function () {
    var $sidebar = $(this.firstNode);    
    $sidebar.find('.thumbnails').height($(window).height() - 180).width($sidebar.width());
    $sidebar.affix({
        offset: 15
    });
    if ($(window).width() < 700){
        $sidebar.css("position","relative");
    }
    if ($(window).scrollTop() >= 15 ){        
        $sidebar.addClass('affix');         
    }
    if(!$(Session.get("activeTab")).hasClass('active') && !_.isEmpty(Session.get("activeTab"))){        
        $('a[href="#friends"]').parent().toggleClass('active');
        $('#friends').toggleClass('active');
        $('a[href="#groups"]').parent().toggleClass('active');
        $('#groups').toggleClass('active');
    }
};
Template.peopleToAdd.events({
    'keyup #query, click': function (e) {          
    if($('#friends').hasClass('active')){
        // Meteor.call('filterFriends', $(e.target).val(), function(err, res) {
        //     var arr = _.map(res, function(obj) {
        //         return {
        //             id: "" + obj.uid,
        //             name: obj.name,
        //             src: "http://graph.facebook.com/" + obj.uid + "/picture/",                
        //         };
        //       });            
        //     Session.set("filteredFriends", arr);
        // });
        var q = $(e.target).val();
        var query = "SELECT name, uid FROM user WHERE uid IN(SELECT uid2 FROM friend WHERE uid1 = me()) AND strpos(lower(name), lower('" + q + "')) >=0 LIMIT 10";
        var arr;
        if (Session.get("FBLoaded")) {
            FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here
             arr = _.map(response.data, function(obj) {
                  return {
                      id: "" + obj.uid,
                      name: obj.name,
                      src: "http://graph.facebook.com/" + obj.uid + "/picture/"           
                  };
                });
                Session.set("filteredFriends", arr);
            });                 
        }
        Session.set("activeTab", "#friends");
      } else {
         var q = $(e.target).val();
         var query = "select gid, name from group where gid in (select gid from group_member where uid = me()) AND strpos(lower(name), lower('" + q + "')) >=0 LIMIT 15";
        // Meteor.call('filterGroups', $(e.target).val(), function(err, res){
        //     Session.set('filteredGroups', res);
        // })     
        if (Session.get("FBLoaded")) {
            FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here
                Session.set("filteredGroups", response.data);
            });                 
        }   
        Session.set("activeTab", "#groups");
      }
    },
    'click .inviteFriends': function() {
        if (Session.get("FBLoaded")) {
            FB.ui({
              method: 'send',
              link: 'http://findtime.co',
              app_id: '517570874963582',
              redirect_uri: 'http://findtime.co'
            });
            ga('send', {
              'hitType': 'event',          // Required.
              'eventCategory': 'sidebar',   // Required.
              'eventAction': 'click',      // Required.
              'eventLabel': 'invite any friend'              
            });
        }
        // window.open( "https://www.facebook.com/dialog/send?app_id=517570874963582&link=http://findtime.co&redirect_uri=http://findtime.co&display=popup", 'facebookSendDialog', "status = 1, height = 300, width = 575, resizable = 0" );
    }
});
Template.sideBarProfile.events({
    click: function (e) {
        var $target = $(e.target).parent().prev(),
            $t = $(e.target),
        obj = {
            id: $target.attr("data-id"),
            title: $target.attr("alt"),
            src: $target.attr("src")
        },prev;              
        if (obj.id) {
            if ($t.hasClass("isUser")) {
                prev = Session.get("selectedFriends");
                prev.push(obj);
                //console.log(prev);
                Session.set("selectedFriends", prev);
                ga('send', {
                  'hitType': 'event',          // Required.
                  'eventCategory': 'sidebar',   // Required.
                  'eventAction': 'click',      // Required.
                  'eventLabel': 'add friend',
                  'eventValue': prev.length
                });                
            }                     
        }
        

    }
});
Template.sideBarProfile.helpers({
    inFriends: function () {
        return Meteor.Router.page() === 'friendSchedule';
    }
});
Template.sideBarCityMate.events({
    "click": function (e) {
        // ...
        var $target = $(e.target).parent().prev(),
            $t = $(e.target),
        obj = {
            id: $target.attr("data-id"),
            title: $target.attr("alt"),
            src: $target.attr("src")
        },prev;              
        if (obj.id) {
            if ($t.hasClass("isntUser")){    
                if (Session.get("FBLoaded")) {
                    FB.ui({
                      method: 'send',
                      link: 'http://findtime.co',
                      app_id: '517570874963582',
                      redirect_uri: 'http://findtime.co',
                      to: obj.id
                    });
                    ga('send', {
                      'hitType': 'event',          // Required.
                      'eventCategory': 'sidebar',   // Required.
                      'eventAction': 'click',      // Required.
                      'eventLabel': 'invite specific user',
                      'eventValue': obj.id
                    });
                }        
                // window.open( "https://www.facebook.com/dialog/send?app_id=517570874963582&link=http://findtime.co&redirect_uri=http://findtime.co&display=popup&to=" + obj.id, 'facebookSendDialog', "status = 1, height = 300, width = 575, resizable = 0" );      
            }
        }
    }
});

Template.sidebarGroups.events({
    'click a.seeGroupSchedule': function (e) {
        var $caption = $(e.target).parent(),
            id = $caption.attr("data-id"),
            friendsOnFindTime;
        // Meteor.call('findGroupMembers', id, function (err, res){            
        //     Session.set('selectedFriends', res);
        // });
        var query = "SELECT uid, name FROM user WHERE is_app_user AND uid IN (SELECT uid FROM group_member WHERE gid = " + id + ")";
        var arr;
        FB.api('fql', { q:  query, access_token: Meteor.user().services.facebook.accessToken }, function (response){
             //Logic here
             arr = _.map(response.data, function(obj) {
                  return {
                      id: "" + obj.uid,
                      name: obj.name,
                      src: "http://graph.facebook.com/" + obj.uid + "/picture/"               
                  };
                });
                Session.set("selectedFriends", arr);
                ga('send', {
                  'hitType': 'event',          // Required.
                  'eventCategory': 'sidebar',   // Required.
                  'eventAction': 'click',      // Required.
                  'eventLabel': 'add group',
                  'eventValue': arr.length
                });
        });   
    },
    'click a.inviteGroup': function (e) {
        var $caption = $(e.target).parent(),
            id = $caption.attr("data-id");
        if (Session.get("FBLoaded")) {
                FB.ui({
                  method: 'send',
                  link: 'http://findtime.co',
                  app_id: '517570874963582',
                  redirect_uri: 'http://findtime.co',
                  to: id                  
                });
                ga('send', {
                  'hitType': 'event',          // Required.
                  'eventCategory': 'sidebar',   // Required.
                  'eventAction': 'click',      // Required.
                  'eventLabel': 'invite group',
                  'eventValue': id                                      
                });
        }  
        // window.open( "https://www.facebook.com/dialog/send?app_id=517570874963582&link=http://findtime.co&redirect_uri=http://findtime.co&display=popup&to=" + id, 'facebookSendDialogGroups', "status = 1, height = 300, width = 575, resizable = 0" );
    }
});
Template.newSched.created = function () {    
    ga('send', 'pageview', {
        'page': "/" + Meteor.Router.page(),
        'title': Meteor.Router.page()        
      });
};
Template.newSched.helpers({
    schedules: function () {                
        var queryArr;        
        Session.setDefault("FBEvents",{courses: {}, vent: []});
        //getEvents();
        if (Session.get("selectedFriends").length > 0) 
            queryArr =_.pluck(Session.get("selectedFriends"), 'id');   
        // if (Meteor.Router.page() === "newHome" || Meteor.Router.page() === "friendSchedule"){            
        //     Session.set("colorCode", _.extend({}, format(Schedules.find({"userId": {$in: queryArr}}).fetch()).courses, Session.get("FBEvents").courses));    
        // } else {
            Session.set("colorCode", format(Schedules.find({"userId": {$in: queryArr}}).fetch()).courses);    
        // }  
        //return (Meteor.Router.page() === "newHome" || Meteor.Router.page() === "friendSchedule") ? merge(format(Schedules.find({"userId": {$in: queryArr}}).fetch()), Session.get("FBEvents")) : format(Schedules.find({"userId": {$in: queryArr}}).fetch());
        return format(Schedules.find({"userId": {$in: queryArr}}).fetch());
    },
    daylist: function () {
        return ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    },
    stringify: function (obj) {
        return JSON.stringify(obj);
    },
    onePerson: function () {
        var queryArr;
        if (Session.get("selectedFriends").length > 0) 
            queryArr =_.pluck(Session.get("selectedFriends"), 'id');        
        return Schedules.find({"userId": {$in: queryArr}}).count() === 1;
    },
    AM: function () {
        return [7, 8, 9, 10, 11, 12];
    },
    PM: function() {
        return [1, 2, 3, 4, 5, 6, 7, 8];
    }
});
Template.newSched.rendered = function () {
    var $fixedContainer = $('.fixedContainer'),
        $fixedTable = $fixedContainer.find('.fixedTable'),
        $fixedHead = $fixedContainer.find('.fixedHead');
    $fixedTable.scroll(function () {
        var x = $fixedTable.scrollLeft();
        $fixedHead.scrollLeft(x);
    });
    $('.fixedHead').each(function() {
        $(this).width($(this).last().parent().width()).css({top: 45}).affix({
            offset: $(this).position()
        });
    });    
    var selectedTD;
    var daylist = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var selection = function (selector) {
        var $selected = $(selector),
            span = $selected.prop('rowspan'),
            trIndex = $selected.parent().index(),
            time = new TimeRange($selected.find('.content').attr("data-celltime"));
            //time = oldtime.add(new TimeRange(oldtime.end, oldtime.end.addMore(span)));
            //console.log(time);
        // console.log($selected.find('.content').attr("data-celltime"), $selected.index());
        $selected.attr("data-oldstyle", $selected.attr('style')).removeAttr('style').find('.forpopovers').attr("data-eventtime", $selected.attr("data-day") + " " + time.toString());
    };
    var serialize = function (selectors) {
        var split = _.map(selectors, function (el, i) {
            return $(el).attr("data-eventtime").split(" ");
        }),
            grouped = _.groupBy(split, function (arr) {
                return arr[0];
            }),
            out = {};            
        _.each(grouped, function (v, k) {
            var output = [],
                vals = _.map(v, function (arr) {                    
                    return new TimeRange(arr[1]);
                }),
                lastTime = _.first(vals);
            if (vals.length > 1) {
                //vals.shift();                    
                _.each(vals, function (value) {
                    if (lastTime.addable(value)) {
                        lastTime = lastTime.add(value);
                    } else {
                        output.push(lastTime);
                        lastTime = value;
                    }
                });
                if (!lastTime.end.equals(_.last(output))) {
                    output.push(lastTime);
                }
            } else {
                output.push(lastTime);
            }                        
            out[k] = output;
            if (output.length > 1)
                output.shift();
        });
        return out;
    };
    if (Meteor.Router.page() !== "friendSchedule"){
        $('.fixedContainer').find('.fixedTable').selectableScroll({
            filter: "td:not(.enormous)",
            selected: function (ev, ui) {
                selection(ui.selected);
            },
            selecting: function (ev, ui) {
                var $uiselecting = $(ui.selecting),
                    forpopovers = $uiselecting.find('.forpopovers').first();
                if (!$uiselecting.attr("selected")) {
                    $uiselecting.attr("selected", "true");
                    $uiselecting.attr("data-oldstyle", $uiselecting.attr("style"));             
                    forpopovers.attr("data-oldhtml", forpopovers.html());                               
                }
                selection(ui.selecting);

            },
            // unselecting: function (ev, ui) {
            //     var $uiselecting = $(ui.unselecting);
            //     selection(ui.unselecting, '#rgb(200, 100, 100)');
            //     // $uiselecting.css({
            //     //     "backgroundColor": '#FFF'
            //     // }).text("");
            //     // $uiselecting.attr("style", $uiselecting.attr("data-oldstyle"));
            //     // $uiselecting.html($uiselecting.attr("data-oldhtml"));
            //     // $uiselecting.attr("selected", "");
            // },
            unselected: function (ev, ui) {
                var $uiselecting = $(ui.unselected),
                forpopovers = $uiselecting.find('.forpopovers'),
                oldhtml = forpopovers.attr("data-oldhtml");                   
                forpopovers.html(oldhtml);
                $uiselecting.css({
                   "backgroundColor": '#FFF'
                });
                $uiselecting.attr("style", $uiselecting.attr("data-oldstyle"));            
                $uiselecting.attr("selected", "").css('color', '#000');
            },
            stop: function (ev, ui) {            
                var $this = $(this),
                    selectedTD = $this.find('.ui-selected'),                
                    forpopovers = selectedTD.last().find('.forpopovers'),
                    data = serialize(selectedTD.find('.forpopovers')), 
                    keys = _.keys(data),                    
                    len = keys.length,                
                    spanSize = len > 3 ? Math.floor(24/len) : Math.floor(12/len),                                
                    finalData = '',
                    popupLen = Meteor.Router.page() === "newHome" ? 300 : 371,
                    inputTmpl = function(v, txt) {
                        var out = "" + txt;
                        out += '<div class="input-append bootstrap-timepicker">';
                        out +=  '<input type="text" class="input-small tpicker" data-default-time="' + v + '"/>';
                        out +=  '<span class="add-on">';
                        out +=     '<i class="icon-time"></i>';
                        out +=  '</span>';
                        out += '</div>';
                        return out;
                    };
                    Session.set("selectedTimes", data);
                    // finalData += '<form class="form-horizontal">';
                    // finalData +=     '<div class="control-group">';
                    // finalData +=        '<label class="control-label" for="inputEvent">Event</label>';
                    // finalData +=        '<div class="controls">';
                    // finalData +=           '<input type="text" id="inputEvent" placeholder="Event">';
                    // finalData +=        '</div>';
                    // finalData +=     '</div>';
                    // finalData +=     '<div class="control-group">';
                    // finalData +=        '<label class="control-label" for="inputLocation">Location</label>';
                    // finalData +=        '<div class="controls">';
                    // finalData +=          '<input type="password" id="inputLocation" placeholder="Location">';
                    // finalData +=        '</div>';
                    // finalData +=      '</div>';
                    // finalData +=  '</form>';
                    // Times
                    // finalData += '<div class="container-fluid"><div class="row-fluid">';                                                                  
                    // _.each(data, function(v,k) {                      
                    //     finalData += '<div class="timeContainer span' + spanSize + '"';                    
                    //     if ((len === 4 && keys.indexOf(k) === 2) || (len === 6 && keys.indexOf(k) === 3)) {
                    //         finalData += ' style="margin-left:0;"'
                    //     }                    
                    //     finalData += '>' + "<h3>" + k + "</h3>";
                    //     _.each(v, function(value){
                    //         var time1 = value.start.toString(),
                    //             time2 = value.end.toString();
                    //         // finalData += '<p>Start:<input type="text" value="' + time1 + '" class="timepicker" /></p>';
                    //         // finalData += '<p>End:<input type="text" value="' + time2 + '" class="timepicker" /></p>';    
                    //         finalData += inputTmpl(time1, 'Start: ') + "<br />" + inputTmpl(time2, 'End : ');
                    //     }); 
                    //     finalData += "</div>"                   
                    // });                
                    // finalData += "</div></div>" 
                    
                    // //Details
                    // finalData += '<div class="container-fluid"><div class="row-fluid"><div class="span6">';
                    // finalData += '<form>';
                    // finalData += '<input type="text" placeholder="Event" class="inputEvent">    ';
                    // finalData += '<input type="text" placeholder="Location" class="inputLocation">';
                    // finalData += '<label class="radio">'
                    // finalData += '<input type="radio" name="type" value="Meeting">';
                    // finalData  +=  '<i class="icon-briefcase"></i>Meet up ';
                    // finalData +=  '</label>'
                    // finalData += '<label class="radio">';
                    // finalData += '<input type="radio" name="type" value="Hangout">  <i class="icon-glass"></i>Hang Out';
                    // finalData += '</label>';
                    // finalData += '</form></div>';
                    // finalData += '<div class="span5 offset1"><form>';                                
                    // finalData += '<textarea class="inputMessage" placeholder="Description." style="width:100%;height:120px;"></textarea>';
                    // finalData += '</form>';
                    // finalData += '</div></div></div>'
                    if (Meteor.Router.page() === "newHome"){
                        finalData += " <div class='pull-right'><a href='#' class='btn btn-primary sendreq'>Create Event</a>  <a class='btn cancelreq'>Cancel</a></div><br>";
                    } else {
                        finalData += "<a href='#' class='btn btn-primary sendreq'>Create Class</a>  <a class='btn-info btn editreq'>Edit Class</a> <a class='btn btn-danger delevent'>Delete</a> <a class='btn cancelreq'>Cancel</a><br>";
                    }                    
                    // console.log(forpopovers.attr("data-content", finalData));
                    forpopovers.attr("data-content", finalData).data('popover').setContent();
                    $this.find('.forpopovers').popover('hide');
                    forpopovers.popover('show');                 
                    // $('.tpicker').timepicker({
                    //     minuteStep:1,
                    //     showMeridian: false,
                    //     template: 'modal'
                    // });               
                    forpopovers.siblings('.popover').css('min-width', popupLen).css("color", "#000").on('mousedown', '.sendreq', function(e) {
                        // var $this = $(this).closest('.popover'),
                        //     obj = {};                    
                        // obj.time = [];
                        // $('.timeContainer').each(function (i, el) {
                        //     var $tpicker = $(el).find('.tpicker'),
                        //         $day = $(el).find('h3').text().substring(0, 3);
                        //     obj.time.push({
                        //             day: $day,
                        //             time: new TimeRange(new Time($tpicker.eq(0).val()), new Time($tpicker.eq(1).val()))
                        //     });
                        // });
                        // obj.event = $this.find('.inputEvent').val();
                        // obj.location = $this.find('.inputLocation').val();
                        // obj.type = $this.find('input:checked').val();
                        // obj.description = $this.find('.inputMessage').val();
                        // console.log(obj);                    
                        e.stopImmediatePropagation();                                        
                        forpopovers.popover('hide');
                        var oldhtml = forpopovers.attr("data-oldhtml");                   
                        forpopovers.html(oldhtml);                    
                        // selectedTD.css({
                        //     "color": "#000",
                        //    "backgroundColor": '#FFF'
                        // });
                        selectedTD.removeClass('ui-selected');
                        selectedTD.each(function (i, el) {
                            $(this).attr("style", $(this).attr("data-oldstyle"));                    
                        });                 
                        selectedTD.attr("selected", "");//.css('color', "#000");                        
                        $('#addEventModal').modal('show');
                        ga('send', {
                          'hitType': 'event',          // Required.
                          'eventCategory': 'popover',   // Required.
                          'eventAction': 'click',      // Required.
                          'eventLabel': 'create event',
                          'eventValue': Session.get("selectedFriends").length
                        });                        
                        return false;
                    }).on('mousedown', '.cancelreq', function (e) {
                        e.stopImmediatePropagation();
                        forpopovers.popover('hide');
                        var oldhtml = forpopovers.attr("data-oldhtml");                   
                        forpopovers.html(oldhtml);                    
                        // selectedTD.css({
                        //    "backgroundColor": '#FFF',
                        //    "color": "#000"
                        // });
                        selectedTD.removeClass('ui-selected');
                        selectedTD.each(function (i, el) {
                            $(this).attr("style", $(this).attr("data-oldstyle"));                    
                        });  
                        selectedTD.attr("selected", "");//.css('color', "#000");
                        ga('send', {
                          'hitType': 'event',          // Required.
                          'eventCategory': 'popover',   // Required.
                          'eventAction': 'click',      // Required.
                          'eventLabel': 'cancel'                          
                        });
                        
                        return false;
                    }).on('mousedown', function (e){
                        e.stopImmediatePropagation();
                        return false;
                    }).on('mousedown', '.delevent', function (e) {
                        e.stopImmediatePropagation();                                        
                        forpopovers.popover('hide');                    
                        var weeklySched = Days.parse(Schedules.findOne({"userId": Meteor.user().services.facebook.id}));
                        var schedID = Schedules.findOne({"userId": Meteor.user().services.facebook.id})._id;
                        selectedTD.find('.forpopovers').each(function(k, v){
                            var course = $.trim($(v).prev().attr("data-title")),
                                section = $.trim($(v).prev().attr("data-content"))
                                day = daylist[$(v).parent().index()].substring(0, 3),
                                block = new TimeBlock({
                                    course: $.trim(course),
                                    section: $.trim(section),
                                    time: [8, 9]
                                });
                            weeklySched.remove(day, block);
                        });                        
                        Meteor.call("updateSchedule", schedID, weeklySched.serialize());
                        var oldhtml = forpopovers.attr("data-oldhtml");                   
                        forpopovers.html(oldhtml);                    
                        // selectedTD.css({
                        //     "color": "#000",
                        //    "backgroundColor": '#FFF'
                        // });
                        selectedTD.removeClass('ui-selected');
                        selectedTD.each(function (i, el) {
                            $(this).attr("style", $(this).attr("data-oldstyle"));                    
                        });
                        selectedTD.attr("selected", "");//.css('color', "#000");
                        ga('send', {
                          'hitType': 'event',          // Required.
                          'eventCategory': 'popover',   // Required.
                          'eventAction': 'click',      // Required.
                          'eventLabel': 'delete'                          
                        });
                        return false;
                    }).on('mousedown', '.editreq', function (e) {
                        e.stopImmediatePropagation();                                        
                        forpopovers.popover('hide');                    
                        var weeklySched = Days.parse(Schedules.findOne({"userId": Meteor.user().services.facebook.id}));
                        var schedID = Schedules.findOne({"userId": Meteor.user().services.facebook.id})._id;
                        selectedTD.find('.forpopovers').each(function(k, v){
                            var course = $.trim($(v).prev().attr("data-title")),
                                section = $.trim($(v).prev().attr("data-content"))
                                day = daylist[$(v).parent().index()].substring(0, 3),
                                block = new TimeBlock({
                                    course: $.trim(course),
                                    section: $.trim(section),
                                    time: [8, 9]
                                }),
                                found = weeklySched.find(day, block);
                            weeklySched.remove(day, block);
                        });                        
                        Meteor.call("updateSchedule", schedID, weeklySched.serialize());
                        var oldhtml = forpopovers.attr("data-oldhtml");                   
                        forpopovers.html(oldhtml);                    
                        // selectedTD.css({
                        //     "color": "#000",
                        //    "backgroundColor": '#FFF'
                        // });
                        selectedTD.removeClass('ui-selected');
                        selectedTD.each(function (i, el) {
                            $(this).attr("style", $(this).attr("data-oldstyle"));                    
                        });
                        selectedTD.attr("selected", "");//.css('color', "#000");
                        ga('send', {
                          'hitType': 'event',          // Required.
                          'eventCategory': 'popover',   // Required.
                          'eventAction': 'click',      // Required.
                          'eventLabel': 'edit'                          
                        });
                        return false;
                    });
            }
        }).find('.forpopovers').popover({
            html: 'true',
            trigger: 'manual',
            title: 'You found time time to set it up'
        });  
    }
      
};
Template.multiUserCell.helpers({
    getImage: function (id) {
        return new Handlebars.SafeString("<img class='profile45' src='http://graph.facebook.com/" + id + "/picture/' />  ");
    },
    getBGStyle: function (day) {        
        var len = Session.get("selectedFriends").length;
        dlen = day.length,
        rgbnum = Math.floor((255 * (len - dlen)) / len),
        color = rgbnum < 170 ? "#FFF" : "#000";
        return "background-color: rgb(" + rgbnum + ", " + rgbnum + ", " + rgbnum + ");";
    },
    getFontStyle: function (day) {
        var len = Session.get("selectedFriends").length;
        dlen = day.length,
        rgbnum = Math.floor((255 * (len - dlen)) / len),
        color = rgbnum < 170 ? "#FFF" : "#000";
        return "color: " + color + ";";
    },
    getStyle: function (day) {
        var len = Session.get("selectedFriends").length;
        dlen = day.length,
        rgbnum = Math.floor((255 * (len - dlen)) / len),
        color = rgbnum < 170 ? "#FFF" : "#000";
        return "background-color: rgb(" + rgbnum + ", " + rgbnum + ", " + rgbnum + "); color: " + color + ";";
    },
    enormous: function () {
        return this.span > 1;
    }
});
Template.multiUserCell.rendered = function () {    
    if (this.data.profiles.length > 0 && this.data.span > 0) {
        var $details = $(this.find('.image-details'));
        $details.attr("data-content", this.find('.content').innerHTML + '<br>');
        $details.popover({
            html: true,
            trigger: "manual",
            title: "Busy"
        });
    }
    
};
Template.multiUserCell.events({
    'mouseenter': function(event) {        
        $(event.target).find('.image-details').popover('show');  
    },
    'mouseleave': function (event){
        $(event.target).find('.image-details').popover('hide');  
    }
});
Template.singleUserCell.helpers({    
    getBGStyle: function (day) {        
        var queryArr, val;
        if (Session.get("selectedFriends").length > 0) 
            queryArr =_.pluck(Session.get("selectedFriends"), 'id'); 
        //return day[0].course ?  : "background-color:#FFF;";        
        if (day.length > 0) {
            val = "background-color: " + Session.get("colorCode")[day[0].course] + ";";
        } else {
            val = "background-color: #FFF";
        }
        return val;
    },
    getFontStyle: function (day) {
        return "color:#000;font-size:0.75em;";
    },    
    truncate: function (str, num) {
        return str.substring(0, num);
    },
    spannable: function () {
        return !(Meteor.Router.page() === "myschedule" || Meteor.Router.page() === "thanks");
    },
    nonspan: function () {
        return this.span === 0 && (Meteor.Router.page() === "myschedule" || Meteor.Router.page() === "thanks");
    },
    enormous: function () {
        return this.span > 1;
    },
    smallSpan: function (span) {
        return span < 3;
    }
});
Template.singleUserCell.rendered = function () {    
    if (this.data.profiles.length > 0) {
        var $details = $(this.firstNode).find('.details');
        $details.popover({
            trigger: "manual"
        });
    }
    
};
Template.singleUserCell.events({
    'mouseenter': function(event) {        
        $(event.target).find('.details').popover('show');  
    },
    'mouseleave': function (event){
        $(event.target).find('.details').popover('hide');  
    }
});
Template.newHome.created = function () {
    var obj;
    if (Meteor.userId)
        obj = {
            title: Meteor.user().profile.name,
            id: Meteor.user().services.facebook.id,
            src: "http://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture/"
        };
    Session.set('selectedFriends', [obj]);
};
Template.newHome.rendered = function () {
    var tour = new Tour({
        onEnd: function (tour) {
            window.localStorage.setItem("newHome_tour_ended", true);
        }
    });
    tour.addSteps([
        {
            element: "h3:first",
            title: "Home",
            content: "Here you can click on your friends to compare schedules and set up meetings and hang outs.",
            placement: "bottom"
        },
        {
            element:'.isUser:first',
            title: "Compare with friends",
            content: "Click on your friend's schedule in order to see what he's up to",
            placement: "left",
            reflex: true
        },
        {
            element: ".profile-bottom:first",
            title: "When you're finished comparing",
            content: "Once you're done click on your friend's picture to take the person out of comparison",
            placement: "top"
        },
        {
            element: "a[href=#groups]",
            title: "Groups",
            content: "Your Facebook groups are automatically imported to FindTime. Click on See Group's Schedule to see common areas of free time for meetings\nNote: Members of the Facebook group must post their schedule on FindTime",
            placement: "left",
            onShow: function (tour){
                $('a[href=#groups]').trigger('click');
            }
        },
        {
            element: ".clear-all",
            title: "Clear",
            content: "Removing each person out of comparison is a hassle, so here's a button to help you clear them all.",
            reflex: true,
            placement: "top"
        },
        {
            element: ".help:first",
            title: "Help",
            content: "If you ever get lost, click Help",
            reflex: true,
            placement: "top"
        }
    ]);
    if (!window.localStorage.getItem("newHome_tour_ended")){
        tour.restart();
    }    
};
Template.friendSchedule.helpers({
    name: function () {
        return Session.get("selectedFriends")[0].title;
    }
});
Template.AddEventModal.helpers({
    timesFormat: function (data) {
        var keys = _.keys(data),                    
            len = keys.length,                
            spanSize = len > 3 ? Math.floor(24/len) : Math.floor(12/len),
            finalData = '',
            daylist = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            inputTmpl = function(v, txt) {
                var out = "<p>" + txt + "</p>";
                out += '<div class="input-append bootstrap-timepicker">';
                out +=  '<input type="text" class="input-small tpicker" data-default-time="' + v + '"/>';
                out +=  '<span class="add-on">';
                out +=     '<i class="icon-time"></i>';
                out +=  '</span>';
                out += '</div>';
                return out;
            },
            timeString = function (obj) {
                var minute,
                    meridian = obj.hour > 12 ? "PM" : "AM",
                    hour;
                if (obj.minutes > 9) {
                    minute = obj.minutes.toString();
                } else {
                    minute = "0" + obj.minutes.toString();
                }

                  if (obj.hour % 12 === 0 ) {
                    hour = 12;
                  } else {
                    hour = obj.hour % 12;
                  }
                return "" + hour + ":" + minute + " " + meridian;
                //return "" + obj.hour + ":" + obj.minutes;
            },
            timeRangeString = function (obj) {
                return timeString(obj.start) + "-" + timeString(obj.end);
            };
            if (len > 0) {
                // console.log(data);
                var newData = {};
                _.each(data, function (v, k) {
                    _.each(v, function (val) {
                        // console.log(val);
                        var time = timeRangeString(val);                                       
                        // console.log(time);
                        if (newData.hasOwnProperty(time)) {
                            newData[time].push(k); 
                        } else {
                            newData[time] = [k];
                        }
                    });             
                });                
                _.each(newData, function(v,k) {                      
                    // finalData += '<div class="timeContainer span' + spanSize + '"';                    
                    // if ((len === 4 && keys.indexOf(k) === 2) || (len === 6 && keys.indexOf(k) === 3)) {
                    //     finalData += ' style="margin-left:0;"'
                    // }                    
                    // finalData += '>' + "<h3>" + k + "</h3>";
                    finalData += "<div class='row-fluid'>";
                    finalData += '<div class="timeContainer span6">';     
                    // _.each(v, function(value){
                    //     // var time1 = timeString(value.start),
                    //     //     time2 = timeString(value.end);                            
                    //     // // finalData += '<p>Start:<input type="text" value="' + time1 + '" class="timepicker" /></p>';
                    //     // // finalData += '<p>End:<input type="text" value="' + time2 + '" class="timepicker" /></p>';    
                    //     // finalData += inputTmpl(time1, 'Start: ') + "<br />" + inputTmpl(time2, 'End : ');

                    // }); 
                    var time1 = k.split("-")[0],
                        time2 = k.split("-")[1];
                    finalData += inputTmpl(time1, 'Start: ') + "<br />" + inputTmpl(time2, 'End : ');
                    finalData += "</div>";
                    finalData += '<div class="span6">';
                    finalData += '<form>';

                    if (Meteor.Router.page() === "newHome") {
                        _.each(v, function (day){
                            var daylist = {
                                    "Sun":0,
                                    "Mon":1,
                                    "Tue":2,
                                    "Wed":3,
                                    "Thu":4,
                                    "Fri":5,
                                    "Sat":6
                                },                                
                                toDate = function(dInWeek) {
                                    var DAY = daylist[dInWeek.substring(0, 3)],
                                        currentDate = new Date(),
                                        currentDay = currentDate.getDay(),
                                        currentDateVal = currentDate.valueOf();
                                    if (currentDay > DAY) {
                                        currentDateVal += (7 - (currentDay - DAY)) * (1000 * 60 * 60 * 24);
                                    } else {
                                        currentDateVal += (DAY - currentDay) * (1000 * 60 * 60 * 24);
                                    }
                                        currentDate = new Date(currentDateVal);
                                        // console.log(currentDate.format("c"), DAY, currentDay);
                                    return currentDate.format('m-d-Y');
                                };
                                finalData += '<div class="input-append date" class="datepicker" data-date-format="mm-dd-yyyy"><input type="text" class="input-small" value="' + toDate(day) +'"><span class="add-on"><i class="icon-calendar"></i></span><button class="delete-date btn btn-danger">x</button></div>';
                                

                        });
                        finalData += '<a class="btn add-new-date">Add Another Day</a>';
                    } else {
                        _.each(daylist, function (day){
                            var checked = _.contains(v, day) ? ' checked="checked"' : '';
                            // console.log(v);
                            // console.log(day);
                            finalData += '<label class="checkbox">';
                            finalData += '<input type="checkbox" value="' + day.substring(0, 3) + '"' + checked + '>' + day;
                            finalData += '</label>';
                        });    
                    }
                                    
                    finalData += '</form>';
                    finalData += "</div></div>";    
                });   
            }                        
        return new Handlebars.SafeString( finalData );
    },
    times: function() {
        return Session.get("selectedTimes") || {};
    },
    isComparing: function() {
        return Meteor.Router.page() === "newHome";
    }
});
Template.AddEventModal.rendered = function () {
    $('.tpicker').timepicker({
                    minuteStep:1,
                    showMeridian: true
                });
    if (Meteor.Router.page() === "newHome") {
        $('.date').datepicker({
            daysOfWeekDisabled: '0'
        });
    }
};
Template.AddEventModal.events({
    "click .addEvent": function () {
            var $this = $('#addEventModal').find(".modal-body"),
                obj = {},
                daylist = {
                    "Sun":0,
                    "Mon":1,
                    "Tue":2,
                    "Wed":3,
                    "Thu":4,
                    "Fri":5,
                    "Sat":6
                },
                time = [],
                toFBDate = function(date, t) {
                    var currentDate = date;                    
                    currentDate.setHours(t.hour);
                    currentDate.setMinutes(t.minutes);
                        // console.log(currentDate.format("c"), DAY, currentDay);
                    return currentDate.format('c');
                },
                toTime = function(str) {
                    var arr = str.split(" "),
                        time = new Time(arr[0]);
                    if (arr[1] === "PM" && time.hour < 12 ) {
                        time.hour += 12;
                    }
                    return time;
                };
            obj.name = $this.find('.inputEvent').val();
            obj.location = $this.find('.inputLocation').val();
            //obj.type = $this.find('input:checked').val();
            obj.description = $this.find('.inputMessage').val();
            obj.privacy_type = "SECRET";
            var d = Schedules.findOne({"userId": Session.get("selectedFriends")[0].id}),
                        days = Days.parse(d);
            $('.timeContainer').each(function (i, el) {
                var $tpicker = $(el).find('.tpicker');                
                if (Meteor.Router.page() === "newHome") {     
                    $(el).next().find('.date').each(function (i, datepicker){
                            var ids = _.pluck(Session.get("selectedFriends"), 'id'),
                                $day = $(datepicker).datepicker('getDate'),
                                success = false;
                            obj.start_time = toFBDate($day, toTime($tpicker.eq(0).val()));
                            obj.end_time = toFBDate($day, toTime($tpicker.eq(1).val()));
                            // Meteor.call("newEvent", obj, ids, function (err, res) {
                            //     if (res) {
                            //         bootbox.alert("Facebook Event Created");
                            //     }
                            // });
                            // FBGraph.post("/me/events", obj, function(err, res) {
                            //     console.log(err)
                            //     if(!err) {
                            //       console.log(res);
                            //       FBGraph.post(res.id + "/invited?users=" + arr.join(","), function(err, result) {
                            //         future.ret(result);
                            //         console.log(result);
                            //       })
                            //     }
                            //   });
                            // console.log(obj);
                            if (Session.get("FBLoaded")) {
                                obj.access_token = Meteor.user().services.facebook.accessToken;
                                FB.api('/me/events', 'post', obj, function(response) {                            
                                  if (!response.hasOwnProperty("error")) {          
                                    // console.log(response); 
                                    var url ='/' + response.id + '/invited?users=' + ids.join(",") + '&access_token=' + Meteor.user().services.facebook.accessToken;                 
                                    // console.log(url);
                                    FB.api(url, 'post', function (result){
                                        // console.log(result);
                                        if (result) {
                                            bootbox.alert("Facebook Event Created");
                                            success = true;
                                        } else {
                                            bootbox.alert('Error occured');
                                        }
                                    });
                                  } else {
                                    bootbox.alert('Error occured');
                                  }
                                });
                            }
                        });
                        ga('send', {
                          'hitType': 'event',          // Required.
                          'eventCategory': 'modal',   // Required.
                          'eventAction': 'click',      // Required.
                          'eventLabel': 'Create facebook event',
                          'eventValue': success ? "success" : "failed"
                        });
                        
                } else {     
                    $(el).next().find('input[type=checkbox]').each(function (index, checkbox) {
                            if ($(checkbox).is(":checked")) {
                                var $day = checkbox.value;                        
                                var timeBlock = new TimeBlock({
                                                course: obj.name,
                                                section: obj.location,                                        
                                                time: new TimeRange(toTime($tpicker.eq(0).val()), toTime($tpicker.eq(1).val()))
                                            });
                                    // console.log(timeBlock);
                                    // console.log(days);
                                    days.add($day, timeBlock);
                                    
                                }
                        });       
                   ga('send', {
                      'hitType': 'event',          // Required.
                      'eventCategory': 'modal',   // Required.
                      'eventAction': 'click',      // Required.
                      'eventLabel': 'Create class'                                      
                    });

                }
            });            
            if (Meteor.Router.page() !== "newHome"){
                Meteor.call("updateSchedule", d._id, days.serialize());
            }
            $this.find('.inputMessage').text("");
            // console.log(obj);            
            $('#addEventModal').modal("hide");
    },
    'click .add-new-date': function (e) {
        $('<div class="input-append date" class="datepicker" data-date-format="mm-dd-yyyy"><input type="text" class="input-small" ><span class="add-on"><i class="icon-calendar"></i></span><button class="delete-date btn btn-danger">x</button></div>').insertBefore(e.target);
        $('.date').datepicker();
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'modal',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'add new date'                                      
        });
    },
    'click .delete-date': function (e) {
        $(e.target).parent().datepicker('remove').remove();    
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'modal',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'delete date'                                      
        });    
    },
    'click .help': function (e) {
        var tour = new Tour({
            container: "#addEventModal"
        });
        var commonSteps = [
            {
                element: "h3", // string (jQuery selector) - html element next to which the step popover should be shown
                title: "Creating an Event", // string - title of the popover
                content: "This is where you can create a new event or class",                
                placement: "bottom"
            },
            {
                element: ".tpicker:first",
                title: "Timepicker",
                content: "This is where you can change the time to be more accurate. Click the clock symbol if you want to change the time.",
                placement: "right",
                reflex: true
            }            
        ];
        if (Meteor.Router.page() === "myschedule") {
            commonSteps.push({
                element: "input:checked:first",
                title: "Choose Days When You Have Class At This TIme",
                content: "You can choose any or all of the days when your class applies.",
                placement: "left",
                reflex: true
            });
        } else {
            commonSteps.push({
                element: ".date:first",
                title: "Enter the Date",
                content: "This is where you can enter the when you want to meet.\nYou can add more days where you can meet at that time by clicking Add Another Day or removing a date by clicking the red button with an X.",
                reflex:true,
                placement: "left"
            });
        }
        tour.addSteps(commonSteps);
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'button',   // Required.
          'eventAction': 'click',      // Required.
          'eventLabel': 'Help in Add Event Modalx'
        });
        tour.restart();
    }
});
Template.importer.events({
    'click .import-dlsu': function (e) {
        e.preventDefault();        
        var userId = parseInt($('#dlsuId').val(), 10),
            pass = $('#dlsuPass').val();
        var schedId = Schedules.findOne({userId: Meteor.user().services.facebook.id})._id,
            sched = Days.parse({
                dates: false,
                recurring: true,
                week: false,
                userId: Meteor.user().services.facebook.id,
                days: {
                    Mon: {
                        blocks: [],
                        inWeek: 1
                    },
                    Tue: {
                        blocks: [],
                        inWeek: 2
                    },
                    Wed: {
                        blocks: [],
                        inWeek: 3
                    },
                    Thu: {
                        blocks: [],
                        inWeek: 4
                    },
                    Fri: {
                        blocks: [],
                        inWeek: 5
                    },
                    Sat: {
                        blocks: [],
                        inWeek: 6
                    }
                }
            });
        Meteor.call('queryDLSU', userId, pass, function (err, res) {
          if (res !== "error") {
             var rows = $(res).find('tr');
             for (i = 1; i < rows.length; i++) {
            var row = $(rows[i]).find('td');
            if (row.length === 7) {
                var days = $.trim(row.eq(3).text()).split(""),
                    time = new TimeRange($.trim(row.eq(4).text())),
                    course = $.trim(row.first().text()),
                    section = $.trim(row.eq(5).text()),
                    block = new TimeBlock({
                        time: time,
                        course: course,
                        section: section
                    }),
                    mapper = {
                        M: "Mon",
                        T: "Tue",
                        W: "Wed",
                        H: "Thu",
                        F: "Fri",
                        S: "Sat"
                    },
                    safe = _.isEmpty(_.difference(days, _.keys(mapper)));
                if (safe) {
                    _.each(days, function (day) {
                        sched.add(mapper[day], block);
                    });
                }

            }
        } 
        Meteor.call("updateSchedule", schedId, sched.serialize());
        ga('send', {
          'hitType': 'event',          // Required.
          'eventCategory': 'import',   // Required.
          'eventAction': 'success',      // Required.
          'eventLabel': 'DLSU'          
        });
        Meteor.Router.to('/myschedule');
          } else {
            bootbox.alert("Login failed. Please check your User ID and Password, then try again.");
            ga('send', {
              'hitType': 'event',          // Required.
              'eventCategory': 'import',   // Required.
              'eventAction': 'fail',      // Required.
              'eventLabel': 'DLSU'          
            });
        }
        });
        return false;
    }
})
