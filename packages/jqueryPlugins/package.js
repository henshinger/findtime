Package.describe({
    summary: "jQuery plugins inserted into the client.",
});

Package.on_use(function (api) {
    api.add_files(['jquery-ui-custom.js', 'jquery-ui-touch-punch.js', 'jquery-ui-scrollable.js', 'bootbox.js', 'bootstrap-timepicker.js','bootstrap-tour.js', 'datepicker.js'], 'client');
});
