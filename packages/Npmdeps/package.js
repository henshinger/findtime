Package.describe({
    summary: "Facebook fbgraph npm module",
});

Package.on_use(function (api) {
    api.add_files('lib.js', 'server');
});

Npm.depends({
	"fbgraph":"0.2.6",
	"cheerio": "0.11.0"
});