FBGraph = Npm.require("fbgraph");
cheerio = Npm.require("cheerio");
// xmpp = Npm.require("node-xmpp");
// var Emitter = Npm.require('events').EventEmitter;
// var http = Npm.require("http")
// 	, https = Npm.require("https");


// var utils = {};
// utils.parseUserId = function (s) {
// 	return s.replace('@chat.facebook.com', '').replace('-', '');
// }

// utils.buildUserId = function (id) {
// 	return ['-', id, '@chat.facebook.com'].join('');
// }

// utils.capitalize = function (s) {
// 	return s.charAt(0).toUpperCase() + s.slice(1);
// }

// /* Traverse ltx tree and find element */
// utils.findElement = function (root, element) {
// 	if(root.is(element))
// 		return root;
// 	for(var i = 0; i < root.children.length; i++)
// 		if( el = findElement(root.children[i], element))
// 			return el;
// 	return null;
// }


// var fbNameCache = {};
// utils.resolveFacebookName = function (userId, cb) {
// 	if(undefined === cb)
// 		cb = function () {};

// 	if(undefined === fbNameCache[userId]) {
// 		var options = {
// 		    host: 'graph.facebook.com',
// 		    port: 443,
// 		    path: '/' + userId,
// 		    method: 'GET',
// 		};

// 		getJSON(options, function (status, result) {
// 			fbNameCache[userId] = result.name;
// 		});
// 		return userId;
// 	} else
// 		return fbNameCache[userId];
// }

// utils.saveFacebookName = function (userId, name) {
// 	fbNameCache[userId] = name;
// }


// /**
//  * getJSON:  REST get request returning JSON object(s)
//  * @param options: http options object
//  * @param callback: callback to pass the results JSON object(s) back
//  */
// function getJSON (options, onResult) {
//     var prot = options.port == 443 ? https : http;
//     var req = prot.request(options, function(res) {
//         var output = '';
//         res.setEncoding('utf8');

//         res.on('data', function (chunk) {
//             output += chunk;
//         });

//         res.on('end', function() {
//             var obj = JSON.parse(output);
//             onResult(res.statusCode, obj);
//         });
//     });

//     req.on('error', function(err) {
//         //res.send('error: ' + err.message);
//     });
//     req.end();
// };

// var stanzaParser = function stanzaParser (stanza) {
// 	this._stanza = stanza;
// 	this.result = {
// 		id: null,
// 		type: null,
// 		response: this._stanza.toString()
// 	};
// 	this._parse();
// }

// stanzaParser.prototype._parse = function () {
// 	var rootName = this._stanza.getName()
// 		, methodName = 'parse' + utils.capitalize(rootName);

// 	if('function' === typeof this[methodName])
// 		this[methodName]();
// }

// stanzaParser.prototype.parsePresence = function () {
// 	this.result = {
// 		id: this._stanza.attrs.id ? this._stanza.attrs.id : null,
// 		type: 'presence',
// 		event: 'unavailable' === this._stanza.attrs.type ? 'offline' : 'online',
// 		user: utils.parseUserId(this._stanza.attrs.from)
// 	};
// }

// stanzaParser.prototype.parseMessage = function () {
// 	if('chat' !== this._stanza.attrs.type)
// 		return;

// 	if(this._stanza.getChild('composing'))
// 		this.result = {
// 			type: 'status',
// 			event: 'composing',
// 			user: utils.parseUserId(this._stanza.attrs.from)
// 		}
// 	else if(this._stanza.getChild('active') && (body = this._stanza.getChild('body')))
// 		this.result = {
// 			type: 'message',
// 			event: 'received',
// 			user: utils.parseUserId(this._stanza.attrs.from),
// 			message: body.getText()
// 		};
// }

// stanzaParser.prototype.parseIq = function () {
// 	if(undefined !== this._stanza.attrs.id && 0 === this._stanza.attrs.id.indexOf('fbiq') && (child = this._stanza.getChild('own-message'))) {
// 		this.result = {
// 			type: 'message',
// 			event: 'sent',
// 			user: utils.parseUserId(child.attrs.to),
// 			message: child.getChild('body').getText()
// 		};
// 	} else if(this._stanza.getChild('query', 'jabber:iq:roster')) {
// 		this._parseFriendList();
// 	} else if("result" === this._stanza.attrs.type && (child = this._stanza.getChild('vCard'))) {
// 		this.result = {
// 			id: this._stanza.attrs.id ? this._stanza.attrs.id : null,
// 			type: 'vCard',
// 			event: 'result',
// 			user: utils.parseUserId(this._stanza.attrs.from),
// 			name: child.getChild('FN').getText(),
// 			photoType: child.getChild('PHOTO').getChild('TYPE').getText(),
// 			photoData: child.getChild('PHOTO').getChild('BINVAL').getText()
// 		}
// 	}
// }


// stanzaParser.prototype._parseFriendList = function () {
// 	var items = this._stanza.getChild('query', 'jabber:iq:roster').getChildren('item');
// 	var users = {};


// 	for(var i in items) {
// 		users[utils.parseUserId(items[i].attrs.jid)] = items[i].attrs.name
// 		utils.saveFacebookName(utils.parseUserId(items[i].attrs.jid), items[i].attrs.name);
// 	}

// 	this.result = {
// 		id: this._stanza.attrs.id ? this._stanza.attrs.id : null,
// 		type: 'list',
// 		event: 'friends',
// 		friends: users
// 	};

// }
// FacebookChat = function FacebookChat (settings) {
// 	var self = this;
// 	Emitter.call(this);

// 	this.client = null;
// 	this.connected = false;

// 	if(!settings.appId)
// 		throw new Exception('appId missed');
// 	if(!settings.accessToken)
// 		throw new Exception('accessToken missed');
// 	if(!settings.userId)
// 		throw new Exception('userId missed');

// 	this.settings = settings;

// 	self.connect();

// 	process.on('SIGINT', function() {
// 		self.close();
// 	});
// };

// FacebookChat.prototype.__proto__ = Emitter.prototype;

// FacebookChat.prototype.utils = utils;

// FacebookChat.prototype.connect = function() {
// 	var self = this;
	
// 	var client = new xmpp.Client({
// 		jid: utils.buildUserId(this.settings.userID),
// 		api_key: this.settings.appId,
// 		access_token: this.settings.accessToken,
// 		host: 'chat.facebook.com'
// 	});

// 	client.addListener('online', function () {
// 		self.connected = true;
// 		self.emit('connected');
// 	});
// 	this.client = client;

// 	this._bindError();
// 	this._bindStanza();
// };

// FacebookChat.prototype._bindError = function () {
// 	var self = this;

// 	function onError (error) {
// 		if(error == 'XMPP authentication failure'){
// 			self.emit('authenticationError', error);
// 		}
// 		else
// 			console.log(error);
// 	}

// 	this.client.addListener('error', onError);
// };

// FacebookChat.prototype.close = function () {
// 	this.client.end();
// };


// FacebookChat.prototype._bindStanza = function () {
// 	var self = this;
	
// 	this.client.addListener('stanza', function(stanza) {
// 		var stanzaParsed = new stanzaParser(stanza);
// 		if(null !== stanzaParsed.result.type) {
// 			var events = [stanzaParsed.result.type + ':' + stanzaParsed.result.event];
// 			if(null !== stanzaParsed.result.id)
// 				events.push('id:'+stanzaParsed.result.id);

// 			events.forEach(function(event) {
// 				self.emit(event, stanzaParsed.result);
// 			});

// 		} else {
// 			console.log('non parsato');
// 			console.log(stanzaParsed);
// 		}
// 	});
// }

// FacebookChat.prototype.sendMessage = function (targetId, message) {
// 	var el = new xmpp.Element('message',{
// 			to: utils.buildUserId(targetId),
// 			type:'chat'
// 		})
// 		.c('body')
// 		.t(message);
// 	this.client.send(el);
// }

// FacebookChat.prototype.getFriends = function (cb) {
// 	var id = 'getFriends' + Math.floor(Math.random()*9999);
// 	cb = cb || function () {};

// 	var el = new xmpp.Element('iq', {
// 			type: 'get',
// 			id: id,
// 			from: utils.buildUserId(1519737027)
// 		}).c('query', { xmlns: 'jabber:iq:roster' }).up();

// 	this.once('id:'+id, cb);
// 	this.client.send(el);
// }

// FacebookChat.prototype.whoAmI = function (cb) {
// 	this.whoIs(null, cb);
// }

// FacebookChat.prototype.whoIs = function (userId, cb) {
// 	var id = 'whoIs'+ Math.floor(Math.random()*9999);
// 	cb = cb || function () {};

// 	var iqAttrs = {
// 		id: id,
// 		type: 'get'
// 	};

// 	if(userId)
// 		iqAttrs.to = utils.buildUserId(userId);

// 	var el = new xmpp.Element('iq', iqAttrs).c('vcard', { xmlns: 'vcard-temp' }).up();

// 	this.once('id:'+id, cb);
// 	this.client.send(el);
// }