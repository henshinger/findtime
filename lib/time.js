//The Time constructor(similar to classes in classical OOP, but not quite).
//It creates an instance of time in a {minutes:x, hour:y} format.
//It has an equals function to compare to instances of Time.
Date.prototype.format=function(e){var t="";var n=Date.replaceChars;for(var r=0;r<e.length;r++){var i=e.charAt(r);if(r-1>=0&&e.charAt(r-1)=="\\"){t+=i}else if(n[i]){t+=n[i].call(this)}else if(i!="\\"){t+=i}}return t};Date.replaceChars={shortMonths:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],longMonths:["January","February","March","April","May","June","July","August","September","October","November","December"],shortDays:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],longDays:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],d:function(){return(this.getDate()<10?"0":"")+this.getDate()},D:function(){return Date.replaceChars.shortDays[this.getDay()]},j:function(){return this.getDate()},l:function(){return Date.replaceChars.longDays[this.getDay()]},N:function(){return this.getDay()+1},S:function(){return this.getDate()%10==1&&this.getDate()!=11?"st":this.getDate()%10==2&&this.getDate()!=12?"nd":this.getDate()%10==3&&this.getDate()!=13?"rd":"th"},w:function(){return this.getDay()},z:function(){var e=new Date(this.getFullYear(),0,1);return Math.ceil((this-e)/864e5)},W:function(){var e=new Date(this.getFullYear(),0,1);return Math.ceil(((this-e)/864e5+e.getDay()+1)/7)},F:function(){return Date.replaceChars.longMonths[this.getMonth()]},m:function(){return(this.getMonth()<9?"0":"")+(this.getMonth()+1)},M:function(){return Date.replaceChars.shortMonths[this.getMonth()]},n:function(){return this.getMonth()+1},t:function(){var e=new Date;return(new Date(e.getFullYear(),e.getMonth(),0)).getDate()},L:function(){var e=this.getFullYear();return e%400==0||e%100!=0&&e%4==0},o:function(){var e=new Date(this.valueOf());e.setDate(e.getDate()-(this.getDay()+6)%7+3);return e.getFullYear()},Y:function(){return this.getFullYear()},y:function(){return(""+this.getFullYear()).substr(2)},a:function(){return this.getHours()<12?"am":"pm"},A:function(){return this.getHours()<12?"AM":"PM"},B:function(){return Math.floor(((this.getUTCHours()+1)%24+this.getUTCMinutes()/60+this.getUTCSeconds()/3600)*1e3/24)},g:function(){return this.getHours()%12||12},G:function(){return this.getHours()},h:function(){return((this.getHours()%12||12)<10?"0":"")+(this.getHours()%12||12)},H:function(){return(this.getHours()<10?"0":"")+this.getHours()},i:function(){return(this.getMinutes()<10?"0":"")+this.getMinutes()},s:function(){return(this.getSeconds()<10?"0":"")+this.getSeconds()},u:function(){var e=this.getMilliseconds();return(e<10?"00":e<100?"0":"")+e},e:function(){return"Not Yet Supported"},I:function(){var e=null;for(var t=0;t<12;++t){var n=new Date(this.getFullYear(),t,1);var r=n.getTimezoneOffset();if(e===null)e=r;else if(r<e){e=r;break}else if(r>e)break}return this.getTimezoneOffset()==e|0},O:function(){return(-this.getTimezoneOffset()<0?"-":"+")+(Math.abs(this.getTimezoneOffset()/60)<10?"0":"")+Math.abs(this.getTimezoneOffset()/60)+"00"},P:function(){return(-this.getTimezoneOffset()<0?"-":"+")+(Math.abs(this.getTimezoneOffset()/60)<10?"0":"")+Math.abs(this.getTimezoneOffset()/60)+":00"},T:function(){var e=this.getMonth();this.setMonth(0);var t=this.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/,"$1");this.setMonth(e);return t},Z:function(){return-this.getTimezoneOffset()*60},c:function(){return this.format("Y-m-d\\TH:i:sP")},r:function(){return this.toString()},U:function(){return this.getTime()/1e3}}

Time = function Time(str) {
  if (str.indexOf(":") === -1){
    var time = parseInt(str, 10);
    this.minutes = time % 100;
    this.hour = (time - this.minutes) / 100;
  }else {
    this.hour = parseInt(str.substring(0, str.indexOf(":")), 10);
    this.minutes = parseInt(str.substring(str.indexOf(":") + 1), 10);
  }
  return this
}
Time.prototype.wrong = function () {
    return this.serialize() > 24 && 0 > this.serialize();
};
Time.parse = function (num) {
    var deci = (num % 1),
        minutes = deci * 60,
        hour = (num - deci) * 100,
        number = hour + minutes;
    return new Time(number.toString());
};
Time.prototype.toString = function () {
    var minute;
    if (this.minutes >= 9) {
        minute = this.minutes.toString();
    } else {
        minute = "0" + this.minutes.toString();
    }
    return this.hour.toString() + ":" + minute;
};
Time.prototype.toAPString = function () {
    var minute;
    if (this.minutes > 9) {
        minute = this.minutes.toString();
    } else {
        minute = "0" + this.minutes.toString();
    }
  if (this.hour % 12 === 0 ) {
    return 12; 
  } else {
    return this.hour % 12;
  }
    return this.hour.toString() + ":" + minute;
};
Time.prototype.equals = function (time) {
    return this.minutes === time.minutes && this.hour === time.hour;
};
Time.prototype.serialize = function () {
    return this.hour + (this.minutes / 60);
};
Time.prototype.add = function() {
  if (this.minutes >= 55) {
    var h = this.hour + 1;
    return new Time(h + "00");
  } else {
    var t = new Time(this.toString());
    if (t.minutes < 45){
      t.minutes += 15;
      t.hour = this.hour;
    } else {
      t.minutes = 0;
      t.hour = this.hour + 1;
    }
    return t;
  }
};
Time.prototype.addMore = function (times) {
  var oldtime = Time.parse(this.serialize());
  for (i=1; i <= times - 1; i++ ) {
    oldtime = oldtime.add();
  }
  return oldtime;
};

//TimeRange is basically a time that goes from Time(x) to Time(y)
//It has methods like within which checks if the instance of Time passed is within the TimeRange.
TimeRange = function TimeRange() {
    if (typeof arguments[0] === 'string') {
        var range = arguments[0].split("-");
        this.start = new Time(range[0]);
        this.end = new Time(range[1]);
    } else {
        this.start = arguments[0];
        this.end = arguments[1];
    }

}
TimeRange.prototype.toString = function () {
    return this.start.toString() + "-" + this.end.toString();
};
TimeRange.prototype.addable = function (range) {
    return this.end.equals(range.start);
};
TimeRange.prototype.add = function (range) {
    return new TimeRange(this.start, range.end);
};
TimeRange.prototype.within = function (time) {
    var ref = time.serialize();
    return this.start.serialize() <= ref && ref <= this.end.serialize();
};
TimeRange.prototype.withinRange = function (range) {
    return this.within(range.start) && this.within(range.end);
};
TimeRange.prototype.overlappable = function (range) {
    return this.within(range.start) || range.within(this.start);
};
TimeRange.prototype.overlap = function (range) {
    return new TimeRange(Time.parse(Math.max(this.start.serialize(), range.start.serialize())), Time.parse(Math.min(this.end.serialize(), range.end.serialize())));
};
TimeRange.prototype.equals = function (range) {
    return range.start.equals(this.start) && range.end.equals(this.end);
};
TimeRange.prototype.serialize = function () {
    return [this.start.serialize(), this.end.serialize()];
};
TimeRange.prototype.length = function () {
    return (this.end.serialize() - this.start.serialize()) / 5;
};
TimeRange.prototype.cells = function () {
    var start = this.start,
        end = this.end,
        sHour = start.hour,
        sMinutes = start.minutes,
        eHour = end.hour,
        eMinutes = end.minutes;
    if (eMinutes < sMinutes) {
        eHour -= 1;
        eMinutes += 60;
    }
    return ((eHour - sHour) * 12) + ((eMinutes - sMinutes) / 5);
};
TimeRange.parse = function (arr) {
    return new TimeRange(Time.parse(arr[0]), Time.parse(arr[1]));
};
TimeRange.prototype.wrong = function () {
    return this.start.wrong() && this.end.wrong();
};

//Timeblock has a course a section and time. I will have to replace section with location and course with event, but that's it for now.
//Time in TimeBlock is actually a TimeRange. Sorry for any confusion.
TimeBlock = function TimeBlock(option) {
    this.course = option.course || "Break";
    this.section = option.section || "Break";
    this.time = option.time;
    this.priority = option.priority || "Meeting";
}
TimeBlock.prototype.sameCourse = function (block) {
    return this.course === block.course;
};
TimeBlock.prototype.sameSectionAndTime = function (block) {
    return this.section === block.section && this.time.equals(block.time);
};
TimeBlock.prototype.overlap = function (block) {
    return new TimeBlock({
        time: this.time.overlap(block.time)
    });
};
TimeBlock.prototype.overlappable = function (block) {
    var a = this.time,
        b = block.time;
    return a.overlappable(b);
};
TimeBlock.prototype.addTime = function (block) {
    return new TimeBlock({
        time: this.time.add(block.time),
        section: this.section,
        course: this.course,
        priority: this.priority
    });
};
TimeBlock.prototype.addableTime = function (block) {
    return this.time.addable(block.time);
};
TimeBlock.prototype.serialize = function () {
    return {
        course: this.course,
        section: this.section,
        time: this.time.serialize(),
        priority: this.priority
    };
};
TimeBlock.parse = function (obj) {
    return new TimeBlock({
        course: obj.course,
        section: obj.section,
        time: TimeRange.parse(obj.time),
        priority: obj.priority
    });
};
TimeBlock.prototype.wrongTime = function () {
    return this.time.wrong();
};

Day = function Day(inWeek, userId) {
    this.blocks = [];
    this.inWeek = inWeek || 0;
    
}
Day.prototype.push = function (block) {
    this.blocks.push(block);
};
Day.prototype.sameSection = function (day) {
    var same = new Day();
    _.each(this.blocks, function (el, i) {
        _.each(day.blocks, function (element, index) {
            console.log(el);
            if (el.sameSectionAndTime(element)) {
                same.push(el);
            }
        });
    });
    return same;
};
Day.prototype.overlappingBreak = function (day) {
    var a = this.breaks().blocks,
        b = day.breaks().blocks,
        breaks = new Day();
    _.each(a, function (el, i) {
        _.each(b, function (element, index) {
            if (el.overlappable(element)) {
                breaks.push(el.overlap(element));
            }
        });
    });
    return breaks;
};
Day.prototype.overlapAll = function (day) {
    var breaks = this.overlappingBreak(day),
        sameSection = this.sameSection(day),
        sameAll = new Day();
    this.blocks = breaks.blocks.concat(sameSection.blocks);
    this.sort();
    return sameAll.overwriteAll(this.blocks);
};
Day.prototype.sort = function () {
    this.blocks = _.sortBy(this.blocks, function (block) {
        return block.time.start.serialize();
    });
    return this;
};
Day.prototype.overwrite = function (block) {
    this.blocks.splice(this.blocks.length - 1, 1, block);
};
Day.prototype.overwriteAll = function (blocks) {
    this.blocks = blocks;
    return this;
};
Day.prototype.addBlock = function (block) {
    var last = _.last(this.blocks);
    if (last && last.addableTime(block)) {
        if (last.sameCourse(block)) {
            this.overwrite(last.addTime(block));
        } else {
            this.push(block);
        }
    } else {
        //this.push(new TimeBlock({
        //    time: new TimeRange(last.time.end, block.time.start)
        //}));
        this.push(block);
    }
};
Day.prototype.isEmpty = function () {
    return _.isEmpty(this.blocks);
};
Day.prototype.unshift = function (block) {
    this.blocks.unshift(block);
};
Day.prototype.serialize = function () {
    return {
        blocks: _.map(this.blocks, function (tb) {
            return tb.serialize();
        }),
        inWeek: this.inWeek
    };
};
Day.parse = function (arr) {
    var day = new Day();
    day.overwriteAll(_.map(arr.blocks, function (obj) {
        return TimeBlock.parse(obj);
    }));
    day.inWeek = arr.inWeek;
    return day;
};
Day.prototype.wrongTime = function () {
    _.reduce(this.blocks, function (memo, block) {
        return memo && block.wrongTime();
    }, true);
};
/*Day.prototype.resolve = function () {
    var that = this,
        lastTime,
        a = [];
    if (this.blocks.length > 0) {
        this.unshift(new TimeBlock({
            time: new TimeRange(Time.parse(0), this.blocks[0].time.start)
        }));
        lastTime = this.blocks[0].time.end;
        _.each(this.blocks, function (block) {
            if (!block.time.start.equals(lastTime)) {
                a.push(new TimeBlock({
                    time: new TimeRange(lastTime, block.time.start)
                }));
            } else {
                a.push(block);
            }
            lastTime = block.time.end;
        });
        this.overwriteAll(a);
        console.log(this.serialize());
        this.push(new TimeBlock({
            time: new TimeRange(lastTime, new Time(2359))
        }));
    } else {
        this.push(new TimeBlock({
            time: new TimeRange(Time.parse(0), new Time("2359"))
        }));
    }
};*/
Day.prototype.stripBreaks = function () {
    var day = new Day();
    day.overwriteAll(_.filter(this.blocks, function (block) {
        return block.course !== "Break";
    }));
    return day;
};
Day.prototype.combine = function (day) {
    //var out = _.filter(this.blocks, function(block){
    //    return block.class === "Break";
    //}),
    //    noBreaks = _.filter(day.blocks, function(block){
    //    return block.class === "Break";
    //});
    var out = new Day();
    out.overwriteAll(this.stripBreaks.blocks.concat(day.stripBreaks.blocks));
    return out.sort();
};
Day.prototype.breaks = function () {
    var day = new Day(),
        lastTime = Time.parse(7);
    _.each(this.stripBreaks().blocks, function (el, i) {
        if (!lastTime.equals(el.time.start)) {
            day.push(new TimeBlock({
                time: new TimeRange(lastTime, el.time.start)
            }));
        }
        lastTime = el.time.end;
    });
    day.push(new TimeBlock({
        time: new TimeRange(lastTime, new Time("2100"))
    }));
    return day;
};
Day.prototype.combineWithBreaks = function (day) {
    var cons = this.combine(day),
        breaks = cons.breaks(),
        out = new Day();
    return out.overwriteAll(cons.blocks.concat(breaks.blocks)).sort();
};
Day.prototype.resolveConflict = function () {    
    var day = Day.parse(this.serialize());
    day.sort();
    var lastTime = day.blocks[0],
        day2 = Day.parse(this.serialize());    
    day2.blocks = [lastTime];
    _.each(_.rest(day.blocks), function(el) {        
        if (el.overlappable(lastTime)) {
            var temp1, temp2, ts1, ts2, te1, te2;
            temp1 = lastTime;
            temp2 = el;
            ts1 = temp1.time.start.serialize();
            ts2 = temp2.time.start.serialize();
            te1 = temp1.time.end.serialize();
            te2 = temp2.time.end.serialize();
            if (ts2 <= ts1){                
                if (te2 >= te1) {                    
                    day2.overwrite(temp2);
                } else {
                    day2.overwrite(temp2);
                    temp1.time.start = Time.parse(te2);
                    day2.push(temp1);
                }
            } else {
                if (te2 >= te1){
                    day2.overwrite(temp1);
                    temp2.time.start = Time.parse(te1);
                    day2.push(temp2);
                } else {
                    day2.overwrite(temp1);
                }
            }
        } else {
            day2.push(el);
        }
        lastTime = el;
    });
    return day2;
};

Days = function Days(rec, date) {
    var date = new Date(date);
    this.days = {};
    this.recurring = rec || false;
    this.userId = "";
    if (rec) {
        this.dates = false;
        this.week = false;
    } else {
        this.dates = [];
        for (i = 1; i < date.getDay(); i++) {
            this.dates.push(new Date(date - 1000 * 24 * 60 * 60 * (date.getDay() - i)));
        }
        for (i = date.getDay(); i <= 6; i++) {
            this.dates.push(new Date(date - 1000 * 24 * 60 * 60 * (6 - date.getDay())));
        }

        var d = date;
        d.setHours(0, 0, 0);
        d.setDate(d.getDate() + 4 - (d.getDay() || 7));
        var yearStart = new Date(d.getFullYear(), 0, 1);
        this.week = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
    }
}
Days.prototype.overwriteAll = function (days) {
    this.days = days;
    return this;
};
Days.prototype.overwrite = function (day, str) {
    this.days[str] = day;
    return this;
};
Days.prototype.serialize = function () {
    var obj = {
        days: {},
        recurring: this.recurring,
        dates: this.dates,
        week: this.week,
      userId: this.userId
    };
    _.each(this.days, function (day, k) {
        obj.days[k] = day.serialize();
    });
    return obj;
};
Days.prototype.add = function (day, block) {
  var today = this.days[day];
  today.addBlock(block);  
  this.overwrite(today.resolveConflict(), day);
  return this;
};
Days.prototype.remove = function(day, block) {
    var today = this.days[day],
        blocks = today.blocks,
        rejected = _.reject(today.blocks, function(b) {
            return b.course === block.course && b.section === block.section;
        })
  today.overwriteAll(rejected);
  this.overwrite(today, this.days[day]);
  return this;
}
Days.parse = function (obj) {
    var days = obj.recurring ? new Days(obj.recurring) : new Days(obj.recurring, obj.dates[6]),
        mydays = {};
    _.each(obj.days, function (day, k) {
        mydays[k] = Day.parse(day);
    });
    days.userId = obj.userId;
    return days.overwriteAll(mydays);
};
reformat = function(input) {
  var out = [];
  for(i=7; i<=20;i++){
    for(j=0; j<=3; j++){
      var t = Time.parse(i),
          t2 = Time.parse(i);
      t.minutes = j * 15;
      t2.minutes = (j+1) * 15;
      if (t2.minutes >= 60){
        t2 = Time.parse(i+1);
      }
      out.push({
        time: new TimeRange(t, t2),
        people: {
          "Sun":[],
          "Mon":[],
          "Tue":[],
          "Wed":[],
          "Thu":[],
          "Fri":[],
          "Sat":[]
        }
      });
    }
  }
  //console.log(out);
  _.each(input, function(days) {
    var days = Days.parse(days);
    _.each(days.days, function(day, key){
      _.each(day.blocks, function(block){
        _.each(out, function(bl){ 
          if(block.time.withinRange(bl.time)){
            bl.people[key].push({
              course: block.course,
              section: block.section,
              userId: days.userId
            });     
          }
        });
      });
    });
  });
  //console.log(out);
  return _.groupBy(out, function(block){
    return block.time.start.hour;
  });
  // _.each(foo, function(hour){
  //   _(hour).groupBy( function(value) {
  //     return block.time.start.minutes;
  //   })
  // })
};
format = function(input) {
  var tempCourses = [];
  var out = {
    courses: {},
    times: [],
    peeps: []
  };
  for(i=7; i<=20;i++){
    for(j=0; j<=3; j++){
      var t = Time.parse(i),
          t2 = Time.parse(i),
          trange;
      t.minutes = j * 15;
      t2.minutes = (j+1) * 15;
      if (t2.minutes >= 60){
        t2 = Time.parse(i+1);
      }
      trange = new TimeRange(t, t2);
      out.times.push(trange);
      out.peeps.push({
        people: {
          "Sun":{span:1, profiles: []},
          "Mon":{span:1, profiles: []},
          "Tue":{span:1, profiles: []},
          "Wed":{span:1, profiles: []},
          "Thu":{span:1, profiles: []},
          "Fri":{span:1, profiles: []},
          "Sat":{span:1, profiles: []}
        }
      });
    }
  }
  //console.log(out); 
  _.each(input, function(days) {
    var days = Days.parse(days);
    _.each(days.days, function(day, key){
      _.each(day.blocks, function(block){
        _.each(out.times, function(bl, index){ 
          if(block.time.withinRange(bl)){
            tempCourses.push(block.course);            
            out.peeps[index].people[key].profiles.push({
              course: block.course,
              section: block.section,
              userId: days.userId
            });     
          }
        });
      });
    });
  });
  //console.log(out);
  out.times = _.map(out.times, function (time) {
    return time.toString();
  });
  _.each(["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"], function(d) {
    _.each(out.peeps, function(people, i) {
      var ppl = people.people[d], int;
      int = i + 1;
      if (!_.isEmpty(ppl.profiles) && (int < out.times.length)) {
        while (_.isEqual(out.peeps[int].people[d].profiles, ppl.profiles) && int < out.times.length && out.peeps[int].people[d].span > 0) {
               ppl.span += 1;
               out.peeps[int].people[d].span = 0;
               int += 1;
        }
             }
    });
  });
  _.each(_.uniq(tempCourses), function (course) {
    out.courses[course] = 'rgb(' + (Math.floor((256 - 149) * Math.random()) + 150) + ',' + (Math.floor((256 - 149) * Math.random()) + 150) + ',' + (Math.floor((256 - 149) * Math.random()) + 150) + ')';
  })
  
  return out;
  // _.each(foo, function(hour){
  //   _(hour).groupBy( function(value) {
  //     return block.time.start.minutes;
  //   })
  // })
};